#ifndef CONNECTIONS_H
#define CONNECTIONS_H


//#include "includes.h"

/*******************************************************************************/
/*                         Подключение периферии к МК                          */
/*******************************************************************************/

#define PIN_BUTTON_UP       4   // кнопка ВВЕРХ
#define PIN_BUTTON_DOWN     6   // кнопка ВНИЗ
#define PIN_BUTTON_LEFT     8   // кнопка ВЛЕВО
#define PIN_BUTTON_RIGHT    5   // кнопка ВПРАВО
#define PIN_BUTTON_OK       3   // кнопка ОК


#define PIN_CHARGING_STATE  A0  // состояние зарядки


#define PIN_I2C_SDA         A4  // линия I2C SDA
#define PIN_I2C_SCL         A5  // линия I2C SCL


#define PIN_UART_TX         1   // линия UART TX
#define PIN_UART_RX         0   // линия UART RX
#define PIN_UART_RX_IRQ     2   // линия прерывания от UART RX
#define IRQ_UART_RX         0   // номер прерывания от UART RX


#define PIN_SPI_MOSI        11  // линия SPI MOSI
#define PIN_SPI_SCK         13  // линия SPI SCK
#define PIN_AD9833_FSYNC    10  // линия выбора SPI AD9833


#define PIN_PWM             9   // выход ШИМ-сигнала
#define PIN_FREQ_IN         7   // вход частотомера
#define PIN_FREQ_PWR        A3  // питание цепи смещения частотомера


#endif /* CONNECTIONS_H */
