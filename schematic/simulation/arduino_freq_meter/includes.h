#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include <stdint.h>

/*******************************************************************************/

// библиотеки
#include <fastIO.h>
#include <SimpleTimeout.h>
//#include <SimpleMenu.h>
//#include <SimpleDigitalScale.h>
//#include <SimpleSSD1306.h>
//#include <SPI.h>
//#include <MD_AD9833.h>
//#include <avr/pgmspace.h>
//#include <TimerOnePWM.h>

/*******************************************************************************/

// файлы проекта
//#include "buttons.h"
//#include "connections.h"
//#include "devices.h"
//#include "font.h"
//#include "gui.h"
//#include "handlers.h"
//#include "parList.h"
//#include "sett_item.h"
//#include "settings.h"
//#include "uartBD.h"
//#include "freq_meter.h"
//#include "txt/txt1251.h"

/*******************************************************************************/

#define dbg(v)  Serial.print(#v); Serial.print(" = "); Serial.print(v, DEC); Serial.print("  0x"); Serial.println(v, HEX); Serial.flush();
#define dbgf(v) Serial.print(#v); Serial.print(" = "); Serial.println(v); Serial.flush();
#define dbgln()  Serial.println(); Serial.flush();

#ifdef FAST_IO_IMPLEMENTED

#define _pinMode        fastPinMode
#define _digitalWrite   fastDigitalWrite
#define _digitalRead    fastDigitalRead
#define _shiftOut       fastShiftOut

#else

#define _pinMode        pinMode
#define _digitalWrite   digitalWrite
#define _digitalRead    digitalRead
#define _shiftOut       shiftOut

#endif
