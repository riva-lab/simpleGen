#include "freq_meter.h"

void freqValuePrint(float v) {
  //  Serial.print("[raw=");
  //  Serial.print(v);
  //  Serial.print("] ");

  int x = 0, d = 3;
  char p[] = "kM";

  while (v > 1000.0) v /= 1000.0, x++;

  float tmp = v;
  while (tmp > 9.99) tmp /= 10.0, d--;

  Serial.print(v, d);
  Serial.print(" ");
  if (x) Serial.print(p[--x]);
  //  Serial.println();
}

void freqMeterTest() {
  //  freqMeterEnable();
  freqMeterMeasure();

  Serial.print("F = ");
  if (freqMeterValue() > FREQ_METER_FMAX + 5000)
    Serial.print(" > 150 k");
  else
    freqValuePrint(freqMeterValue());
  Serial.println("Hz");
  Serial.println("----------------------------------------");
}

void setup() {
  ADMUX = 0b11000000;
    
  Serial.begin(115200);
  pinMode(8, INPUT_PULLUP);
  freqMeterEnable();

  //  float f = 1.2345678;
  //  freqValuePrint(f * 1e-1);
  //  freqValuePrint(f * 1e0);
  //  freqValuePrint(f * 1e1);
  //  freqValuePrint(f * 1e2);
  //  freqValuePrint(f * 1e3);
  //  freqValuePrint(f * 1e4);
  //  freqValuePrint(f * 1e5);
}

void loop() {
  if (digitalRead(8))
    freqMeterTest();
  else
    freqMeterDisable();

  delay(1000);
}
