#include "Wire.h"
#define PCF8591 (0x90 >> 1) /* I2C адреса, може відрізнятися від вашої, запустіть скетч I2Cscanner
із прикладів бібліотеки Wire, щоб отримати правильну адресу*/
void setup(){
 Wire.begin();
 Serial.begin(115200);
}

void setDAC(uint8_t value){
   Wire.beginTransmission(PCF8591); // починаємо передачу PCF8591
   Wire.write(0x40); // керуюча команда, що вмикає ЦАП (бінарне 1000000)
   Wire.write(value); // значення, що буде видано на AOUT
   Wire.endTransmission(); // закінчили передачу
}

void loop(){
 
    // прием команд по UART
    if (Serial.available()) {
      delay(25);
      uint32_t x = 0;
      uint8_t c = 0;
      
      do {
        if (!Serial.available()) {
            delay(1);
            continue;
        }
        
        c = Serial.read();
        if ((c == 10) || (c == 13)) break;
        
        x *= 10;
        x += c- '0';
      } while (1);
      
      Serial.print("DAC = ");
      Serial.println(x, DEC);
      
      setDAC((uint8_t)((x >> 0) & 0xFF));
    }
}