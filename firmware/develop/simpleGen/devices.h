#ifndef DEVICES_H
#define DEVICES_H

/*******************************************************************************/

#include "includes.h"

/*******************************************************************************/

extern SimpleSSD1306        disp;
extern CustomButtons        btn;
extern MD_AD9833            gen;

/*******************************************************************************/

#define DISPLAY_I2C_SPEED   600     // [кГц] частота шины I2C OLED-дисплея
#define TIME_BATTERY        2000    // [мс]  период обновления уровня заряда АБ

#define ADC_UREF_CALIBRATED 1104    // [мВ] калибровка Uref (1100 мВ) для замера напряжения
#define LIION_VOLTAGE_100   4200    // [мВ] напряжение 100% заряженной Li-Ion АБ
#define LIION_VOLTAGE_80    3950    // [мВ] напряжение 80%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_60    3850    // [мВ] напряжение 60%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_40    3750    // [мВ] напряжение 40%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_20    3700    // [мВ] напряжение 20%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_0     2800    // [мВ] напряжение 0%   заряженной Li-Ion АБ

#define BATTERY_LOW         15      // [%]  нижний уровень заряда аккумулятора

#define UART_BAUDRATE       115200  // скорость соединения с хостом по UART

/*******************************************************************************/

void        commonInit();               // общая инициализация

// управление питанием и энергосбережением
uint8_t     powerIsCharging();
void        powerOffTimersReset(uint8_t reset);
uint8_t     isSleepMode();
void        waitInSleep();

uint16_t    getSystemVoltage();         // замер напряжения питания
uint8_t     liIonBatteryPercent();      // уровень напряжения питания в %

// управление работой генератора
void        genSettings(uint8_t wave, uint32_t freq, uint8_t enable);

// управление работой пъезоизлучателя
void buzzerStart(uint8_t beeps, uint8_t durationX10ms);
void buzzerStop();
void buzzerUpdate();                    // вызывать периодически, почаще

/*******************************************************************************/

#endif /* DEVICES_H */
