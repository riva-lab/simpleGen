#include "settings.h"
#include <EEPROM.h>

/*******************************************************************************/

ProjectSettings_t setting;


void settingsSave() {
  uint8_t *bytes = (uint8_t *)(&setting);

  for (uint8_t i = 0; i < sizeof(setting); i++)
    if (bytes[i] != ~EEPROM.read(SETTINGS_EEPROM_START + i))
      EEPROM.write(SETTINGS_EEPROM_START + i, ~bytes[i]);
}

void settingsRestore() {
  uint8_t *bytes = (uint8_t *)(&setting);

  for (uint8_t i = 0; i < sizeof(setting); i++)
    bytes[i] = ~EEPROM.read(SETTINGS_EEPROM_START + i);
}
