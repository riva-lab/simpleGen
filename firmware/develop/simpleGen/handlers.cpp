#include "handlers.h"

/*******************************************************************************/


float settBrightnessHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.displayContrast = (uint8_t)newValue;
  disp.contrast(setting.displayContrast - 1);
  return setting.displayContrast;
}

float settInversionHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.displayInvert = (uint8_t)newValue;
  disp.invert(setting.displayInvert);
  return setting.displayInvert;
}

float settDisplayOffHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    setting.timeDisplayOff = (uint8_t)newValue * 10;
    powerOffTimersReset(1);
  }
  return setting.timeDisplayOff / 10;
}

float settI2CFreqHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.i2cFrequency = (uint8_t)newValue;
  return setting.i2cFrequency;
}

float settI2CDelayHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.i2cDelay = (uint8_t)newValue;
  return setting.i2cDelay;
}

float settUARTbaudHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.uartBaudrate = (uint8_t)newValue;
  return setting.uartBaudrate;
}

float settUARTtimeoutHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.uartTimeout = (uint8_t)newValue;
  return setting.uartTimeout;
}

float settBuzzerHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.buzzerEnable = (uint8_t)newValue;
  return setting.buzzerEnable;
}

float settBuzzPulseX10Handler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.timerBuzzPulseX10 = (uint8_t)newValue;
  return setting.timerBuzzPulseX10;
}

float settBuzzBeepsHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.timerBuzzBeeps = (uint8_t)newValue;
  return setting.timerBuzzBeeps;
}

float settTimerGPIOHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.timerSignalType = (uint8_t)newValue;
  return setting.timerSignalType;
}

uint32_t settGetUARTbaudrateValue() {
  uint32_t x = 0;
  char *s = getStringPGMTable(strUARTbaud, (uint8_t)settUARTbaudHandler(0, 0));
  while (*s) x *= 10, x += *s - '0', s++;
  return x;
}
