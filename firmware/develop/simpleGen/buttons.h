#ifndef BUTTONS_H
#define BUTTONS_H


#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif


#include <stdint.h>

/*******************************************************************************/

// предустановки обработки кнопок
#define BUTTONS_MAX         5   //
#define BUTTONS_DEBOUNCE    50  // [мс]
#define BUTTONS_PRESSTIME   700 // [мс]

// коды кнопок стандартные
#define BUTTON_NONE         0
#define BUTTON_LEFT         1
#define BUTTON_RIGHT        2
#define BUTTON_UP           3
#define BUTTON_DOWN         4
#define BUTTON_OK           5

// параметр freeState
#define BTN_FREE_IS_GND     0
#define BTN_FREE_IS_VCC     1

// возвращаемое значение getState()
#define BTN_ACTION_WAIT     0
#define BTN_ACTION_NONE     0
#define BTN_ACTION_CLICK    1
#define BTN_ACTION_PRESS    2
#define BTN_ACTION_HOLD     3

/*******************************************************************************/

class CustomButtons {
    
  public:  
    void begin(uint8_t debounce_ms = BUTTONS_DEBOUNCE);
    void reset();
    void poll();

    void addButton(uint8_t pin, uint8_t code, uint8_t freeState = BTN_FREE_IS_VCC);

    uint8_t clicked();
    uint8_t pressed();
    uint8_t holded();

    uint8_t getState(uint8_t pin);
    uint8_t isActivated();
    void    waitForRelease();

  private:
    uint8_t     _debounce;
    uint16_t    _pressTime  = BUTTONS_PRESSTIME;
    uint8_t     _count      = 0;

    struct {
      uint8_t pin;
      uint8_t code;
      uint16_t time;

      unsigned freeState: 1;
      unsigned pinState : 1;
      unsigned activated: 1;
      unsigned click    : 1;
      unsigned press    : 1;
      unsigned hold     : 1;
    } _btn [BUTTONS_MAX] = { 0 };
};

#endif /* BUTTONS_H */
