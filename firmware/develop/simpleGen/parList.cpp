#include "parList.h"

/*******************************************************************************/
/*                               Строковые наборы                              */
/*******************************************************************************/

// список для параметра "ВКЛ/ВЫКЛ/АВТО"
const char* const strOffOnAuto      [] PROGMEM = { TXT_OFF, TXT_ON, TXT_AUTO };

// список для параметра "сигнал таймера"
const char* const strTmrSignal      [] PROGMEM = { TXT_OFF, TXT_NOINV, TXT_INV };

// список для параметра "скорость UART"
const char* const strUARTbaud       [] PROGMEM = { TXT_UART_75,
                                                   TXT_UART_110,
                                                   TXT_UART_134,
                                                   TXT_UART_150,
                                                   TXT_UART_200,
                                                   TXT_UART_300,
                                                   TXT_UART_600,
                                                   TXT_UART_1200,
                                                   TXT_UART_1800,
                                                   TXT_UART_2400,
                                                   TXT_UART_4800,
                                                   TXT_UART_7200,
                                                   TXT_UART_9600,
                                                   TXT_UART_14400,
                                                   TXT_UART_19200,
                                                   TXT_UART_28800,
                                                   TXT_UART_38400,
                                                   TXT_UART_56000,
                                                   TXT_UART_57600,
                                                   TXT_UART_115200,
                                                   TXT_UART_128000
                                                 };

/*******************************************************************************/
/*                                   ДИАЛОГИ                                   */
/*******************************************************************************
  ТИП  | тип
  СТР  | указатель на начало массива строк
  МИН  | мин.значение
  МАКС | макс.значение
  ШАГ  | шаг изменения параметра
  ЗНАК | кол-во знаков после запятой
  ОБР  | обработчик получения/записи параметра
 *******************************************************************************/

#define DEFINE_DIALOG(name)     const dialog_t name PROGMEM = {
#define DEFINE_DIALOG_END       };

DEFINE_DIALOG(dlgBrightness) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     NULL,               1,  16,     1,  0,      settBrightnessHandler },
  PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgInversion) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strOffOnAuto,       0,  1,      1,  0,      settInversionHandler },
  PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgDisplayOff) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_ZERO1,    0,  120,    1,  0,      settDisplayOffHandler },
  PARAM_LABEL(TXT_DELIM_SPACE),
              PARAM_LABEL(TXT_DELIM_SECOND)
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgI2CFreq) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_ZERO1,    1,  70,     1,  0,      settI2CFreqHandler },
  PARAM_LABEL(TXT_DELIM_SPACE),
              PARAM_LABEL(TXT_DELIM_KHZ)
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgI2CDelay) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    5,  125,    5,  0,      settI2CDelayHandler },
  PARAM_LABEL(TXT_DELIM_MS),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgUARTbaud) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strUARTbaud,        0,  20,     1,  0,      settUARTbaudHandler },
  PARAM_LABEL(TXT_DELIM_SPACE),
              PARAM_LABEL(TXT_DELIM_BODS)
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgUARTtimeout) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    1,  30,     1,  0,      settUARTtimeoutHandler },
  PARAM_LABEL(TXT_DELIM_SECOND),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgBuzzer) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strOffOnAuto,       0,  1,      1,  0,      settBuzzerHandler },
  PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgBuzzerParam) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_MULT,     1,  120,    1,  0,      settBuzzBeepsHandler },
  { PARAM_TYPE_NUM,     TXT_DELIM_X10MS,    1,  120,    1,  0,      settBuzzPulseX10Handler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTimerGPIO) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strTmrSignal,       0,  2,      1,  0,      settTimerGPIOHandler },
  PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgInfoA) {
  PARAM_LABEL(TXT_INFO_AUTHOR),
              PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgInfoV) {
  PARAM_LABEL(TXT_INFO_DATE),
              PARAM_EMPTY(),
              PARAM_EMPTY()
} DEFINE_DIALOG_END


/*******************************************************************************/
/*                                 МЕНЮ ПУНКТОВ                                */
/*******************************************************************************/

// меню НАСТРОЙКИ
const settingItem_t siSett[] PROGMEM = {
  { TXT_ITEM_SBRIGH,    &dlgBrightness      },
  { TXT_ITEM_SINV,      &dlgInversion       },
  { TXT_ITEM_SDISPF,    &dlgDisplayOff      },
  { TXT_ITEM_SI2CFRQ,   &dlgI2CFreq         },
  { TXT_ITEM_SI2CDEL,   &dlgI2CDelay        },
  { TXT_ITEM_SUARTBD,   &dlgUARTbaud        },
  { TXT_ITEM_SUARTTM,   &dlgUARTtimeout     },
  { TXT_ITEM_SBUZZER,   &dlgBuzzer          },
  { TXT_ITEM_SBUZSIG,   &dlgBuzzerParam     },
  { TXT_ITEM_STMRSIG,   &dlgTimerGPIO       },
  { TXT_ITEM_SABOUTA,   &dlgInfoA           },
  { TXT_ITEM_SABOUTV,   &dlgInfoV           }
};


/*******************************************************************************/
/*                              МЕНЮ ДОМАШНЕГО ЭКРАНА                          */
/*******************************************************************************/

#define DEFINE_MENU_ITEM(settingItem, name) { sizeof(settingItem) / sizeof(settingItem_t), settingItem, name }

const MenuData_t menuHome[] PROGMEM = {
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_GEN         ),  // генератор на AD9833
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_PWM         ),  // генератор ШИМ
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_FMETER      ),  // частотомер
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_I2C_SCAN    ),  // сканер шины I2C
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_UART_BD     ),  // детектор скорости UART
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_UART_MON    ),  // монитор UART rx
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_TIMER       ),  // таймер
  DEFINE_MENU_ITEM(NULL,    TXT_HDR_STOPWATCH   ),  // секундомер
  DEFINE_MENU_ITEM(siSett,  TXT_HDR_SETT        )   // настройки
};
