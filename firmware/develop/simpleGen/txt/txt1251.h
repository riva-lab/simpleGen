#ifndef TXT_LABELS_H
#define TXT_LABELS_H

// ���� ��������� ����� �������, � ������� �����������.
// ��������! ���� ���� ������ ���� � ��������� Win1251.


#include "versions.h"
#define __mkstr(s)  # s
#define mkstr(s)    __mkstr(s)



/*******************************************************************************/
/*******************************************************************************/

// ���� ����������: 0=EN, 1=RU
#define LANGUAGE            1

#if (LANGUAGE == 0)     // English localization
#define TXT_INFO_LANG       "EN"
#endif

#if (LANGUAGE == 1)     // ���� ���������� �������
#define TXT_INFO_LANG       "RU"
#endif



/*******************************************************************************/
/*******************************************************************************/

// ���������� �� ����������
/*
#define TXT_INFO_DATE           "2021.12.01"
#define TXT_INFO_VERSION        "FW v0.0-EN"
#define TXT_INFO_AUTHOR         "(c) RIVA"
#define TXT_INFO_NAME           "Generator"
*/

const char strInfo[] PROGMEM =
  FW_VERSION_DATE" "           /* TXT_INFO_DATE  L=10 */
  "v" mkstr(FW_VERSION_MAJOR) "." mkstr(FW_VERSION_MINOR) "-" TXT_INFO_LANG"\0"   /* TXT_INFO_VERSION  L=7 */
  "(c) RIVA""\0"                /* TXT_INFO_AUTHOR  L=8 */
  "Generator""\0"               /* TXT_INFO_NAME  L=9 */;

#define TXT_INFO_DATE           (const char *)(strInfo + 0)
#define TXT_INFO_VERSION        (const char *)(strInfo + 11)
#define TXT_INFO_AUTHOR         (const char *)(strInfo + 19)
#define TXT_INFO_NAME           (const char *)(strInfo + 28)

#define TXT_INFO_DATE_LENGTH    10
#define TXT_INFO_VERSION_LENGTH 7
#define TXT_INFO_AUTHOR_LENGTH  8
#define TXT_INFO_NAME_LENGTH    9

/*******************************************************************************/
/*
#define TXT_UART_75             "75"
#define TXT_UART_110            "110"
#define TXT_UART_134            "134"
#define TXT_UART_150            "150"
#define TXT_UART_200            "200"
#define TXT_UART_300            "300"
#define TXT_UART_600            "600"
#define TXT_UART_1200           "1200"
#define TXT_UART_1800           "1800"
#define TXT_UART_2400           "2400"
#define TXT_UART_4800           "4800"
#define TXT_UART_7200           "7200"
#define TXT_UART_9600           "9600"
#define TXT_UART_14400          "14400"
#define TXT_UART_19200          "19200"
#define TXT_UART_28800          "28800"
#define TXT_UART_38400          "38400"
#define TXT_UART_56000          "56000"
#define TXT_UART_57600          "57600"
#define TXT_UART_115200         "115200"
#define TXT_UART_128000         "128000"
#define TXT_UART_230400         "230400"
#define TXT_UART_460800         "460800"
#define TXT_UART_921600         "921600"
*/

const char strUARTbaudList[] PROGMEM =
  "75""\0"                      /* TXT_UART_75  L=2 */
  "110""\0"                     /* TXT_UART_110  L=3 */
  "134""\0"                     /* TXT_UART_134  L=3 */
  "150""\0"                     /* TXT_UART_150  L=3 */
  "200""\0"                     /* TXT_UART_200  L=3 */
  "300""\0"                     /* TXT_UART_300  L=3 */
  "600""\0"                     /* TXT_UART_600  L=3 */
  "1200""\0"                    /* TXT_UART_1200  L=4 */
  "1800""\0"                    /* TXT_UART_1800  L=4 */
  "2400""\0"                    /* TXT_UART_2400  L=4 */
  "4800""\0"                    /* TXT_UART_4800  L=4 */
  "7200""\0"                    /* TXT_UART_7200  L=4 */
  "9600""\0"                    /* TXT_UART_9600  L=4 */
  "14400""\0"                   /* TXT_UART_14400  L=5 */
  "19200""\0"                   /* TXT_UART_19200  L=5 */
  "28800""\0"                   /* TXT_UART_28800  L=5 */
  "38400""\0"                   /* TXT_UART_38400  L=5 */
  "56000""\0"                   /* TXT_UART_56000  L=5 */
  "57600""\0"                   /* TXT_UART_57600  L=5 */
  "115200""\0"                  /* TXT_UART_115200  L=6 */
  "128000""\0"                  /* TXT_UART_128000  L=6 */
  "230400""\0"                  /* TXT_UART_230400  L=6 */
  "460800""\0"                  /* TXT_UART_460800  L=6 */
  "921600""\0"                  /* TXT_UART_921600  L=6 */;

#define TXT_UART_75             (const char *)(strUARTbaudList + 0)
#define TXT_UART_110            (const char *)(strUARTbaudList + 3)
#define TXT_UART_134            (const char *)(strUARTbaudList + 7)
#define TXT_UART_150            (const char *)(strUARTbaudList + 11)
#define TXT_UART_200            (const char *)(strUARTbaudList + 15)
#define TXT_UART_300            (const char *)(strUARTbaudList + 19)
#define TXT_UART_600            (const char *)(strUARTbaudList + 23)
#define TXT_UART_1200           (const char *)(strUARTbaudList + 27)
#define TXT_UART_1800           (const char *)(strUARTbaudList + 32)
#define TXT_UART_2400           (const char *)(strUARTbaudList + 37)
#define TXT_UART_4800           (const char *)(strUARTbaudList + 42)
#define TXT_UART_7200           (const char *)(strUARTbaudList + 47)
#define TXT_UART_9600           (const char *)(strUARTbaudList + 52)
#define TXT_UART_14400          (const char *)(strUARTbaudList + 57)
#define TXT_UART_19200          (const char *)(strUARTbaudList + 63)
#define TXT_UART_28800          (const char *)(strUARTbaudList + 69)
#define TXT_UART_38400          (const char *)(strUARTbaudList + 75)
#define TXT_UART_56000          (const char *)(strUARTbaudList + 81)
#define TXT_UART_57600          (const char *)(strUARTbaudList + 87)
#define TXT_UART_115200         (const char *)(strUARTbaudList + 93)
#define TXT_UART_128000         (const char *)(strUARTbaudList + 100)
#define TXT_UART_230400         (const char *)(strUARTbaudList + 107)
#define TXT_UART_460800         (const char *)(strUARTbaudList + 114)
#define TXT_UART_921600         (const char *)(strUARTbaudList + 121)

#define TXT_UART_75_LENGTH      2
#define TXT_UART_110_LENGTH     3
#define TXT_UART_134_LENGTH     3
#define TXT_UART_150_LENGTH     3
#define TXT_UART_200_LENGTH     3
#define TXT_UART_300_LENGTH     3
#define TXT_UART_600_LENGTH     3
#define TXT_UART_1200_LENGTH    4
#define TXT_UART_1800_LENGTH    4
#define TXT_UART_2400_LENGTH    4
#define TXT_UART_4800_LENGTH    4
#define TXT_UART_7200_LENGTH    4
#define TXT_UART_9600_LENGTH    4
#define TXT_UART_14400_LENGTH   5
#define TXT_UART_19200_LENGTH   5
#define TXT_UART_28800_LENGTH   5
#define TXT_UART_38400_LENGTH   5
#define TXT_UART_56000_LENGTH   5
#define TXT_UART_57600_LENGTH   5
#define TXT_UART_115200_LENGTH  6
#define TXT_UART_128000_LENGTH  6
#define TXT_UART_230400_LENGTH  6
#define TXT_UART_460800_LENGTH  6
#define TXT_UART_921600_LENGTH  6

/*******************************************************************************/
/*******************************************************************************/


#if (LANGUAGE == 0)     // English localization

/*******************************************************************************/
/*
#define TXT_DELIM_SPACE         " "
#define TXT_DELIM_DOT           "."
#define TXT_DELIM_COLON         ":"
#define TXT_DELIM_SECOND        "s"
#define TXT_DELIM_MS            "ms"
#define TXT_DELIM_BODS          "bod/s"
#define TXT_DELIM_ZERO1         "0"
#define TXT_DELIM_KHZ           "kHz"
#define TXT_DELIM_MICROS        "us"
#define TXT_DELIM_BYTE          "bytes"
#define TXT_DELIM_MULT          " x "
#define TXT_DELIM_X10MS         "0 ms"
*/

const char strDelimiter[] PROGMEM =
  " ""\0"                       /* TXT_DELIM_SPACE  L=1 */
  ".""\0"                       /* TXT_DELIM_DOT  L=1 */
  ":""\0"                       /* TXT_DELIM_COLON  L=1 */
  "s""\0"                       /* TXT_DELIM_SECOND  L=1 */
  "ms""\0"                      /* TXT_DELIM_MS  L=2 */
  "bod/s""\0"                   /* TXT_DELIM_BODS  L=5 */
  "0""\0"                       /* TXT_DELIM_ZERO1  L=1 */
  "kHz""\0"                     /* TXT_DELIM_KHZ  L=3 */
  "us""\0"                      /* TXT_DELIM_MICROS  L=2 */
  "bytes""\0"                   /* TXT_DELIM_BYTE  L=5 */
  " x ""\0"                     /* TXT_DELIM_MULT  L=3 */
  "0 ms""\0"                    /* TXT_DELIM_X10MS  L=4 */;

#define TXT_DELIM_SPACE         (const char *)(strDelimiter + 0)
#define TXT_DELIM_DOT           (const char *)(strDelimiter + 2)
#define TXT_DELIM_COLON         (const char *)(strDelimiter + 4)
#define TXT_DELIM_SECOND        (const char *)(strDelimiter + 6)
#define TXT_DELIM_MS            (const char *)(strDelimiter + 8)
#define TXT_DELIM_BODS          (const char *)(strDelimiter + 11)
#define TXT_DELIM_ZERO1         (const char *)(strDelimiter + 17)
#define TXT_DELIM_KHZ           (const char *)(strDelimiter + 19)
#define TXT_DELIM_HZ            (const char *)(strDelimiter + 20)
#define TXT_DELIM_MICROS        (const char *)(strDelimiter + 23)
#define TXT_DELIM_BYTE          (const char *)(strDelimiter + 26)
#define TXT_DELIM_MULT          (const char *)(strDelimiter + 32)
#define TXT_DELIM_X10MS         (const char *)(strDelimiter + 36)

#define TXT_DELIM_SPACE_LENGTH  1
#define TXT_DELIM_DOT_LENGTH    1
#define TXT_DELIM_COLON_LENGTH  1
#define TXT_DELIM_SECOND_LENGTH 1
#define TXT_DELIM_MS_LENGTH     2
#define TXT_DELIM_BODS_LENGTH   5
#define TXT_DELIM_ZERO1_LENGTH  1
#define TXT_DELIM_KHZ_LENGTH    3
#define TXT_DELIM_MICROS_LENGTH 2
#define TXT_DELIM_BYTE_LENGTH   5
#define TXT_DELIM_MULT_LENGTH   3
#define TXT_DELIM_X10MS_LENGTH  4

/*******************************************************************************/
/*
#define TXT_OFF                 "off"
#define TXT_ON                  "on"
#define TXT_AUTO                "auto"
#define TXT_INV                 "on (count = 0)"
#define TXT_NOINV               "on (count = 1)"
*/

const char strItemMenu[] PROGMEM =
  "off""\0"                     /* TXT_OFF  L=3 */
  "on""\0"                      /* TXT_ON  L=2 */
  "auto""\0"                    /* TXT_AUTO  L=4 */
  "on (count = 0)""\0"          /* TXT_INV  L=14 */
  "on (count = 1)""\0"          /* TXT_NOINV  L=14 */;

#define TXT_OFF                 (const char *)(strItemMenu + 0)
#define TXT_ON                  (const char *)(strItemMenu + 4)
#define TXT_AUTO                (const char *)(strItemMenu + 7)
#define TXT_INV                 (const char *)(strItemMenu + 12)
#define TXT_NOINV               (const char *)(strItemMenu + 27)

#define TXT_OFF_LENGTH          3
#define TXT_ON_LENGTH           2
#define TXT_AUTO_LENGTH         4
#define TXT_INV_LENGTH          14
#define TXT_NOINV_LENGTH        14

/*******************************************************************************/
/*
#define TXT_ITEM_SBRIGH         "backlight"
#define TXT_ITEM_SINV           "disp. invert"
#define TXT_ITEM_SDISPF         "display off"
#define TXT_ITEM_SABOUTA        "info: author"
#define TXT_ITEM_SABOUTV        "info: version"
#define TXT_ITEM_SI2CFRQ        "I2C: frequency"
#define TXT_ITEM_SI2CDEL        "I2C: period"
#define TXT_ITEM_SUARTTM        "UART: timeout"
#define TXT_ITEM_SUARTBD        "UART: baudrate"
#define TXT_ITEM_SBUZZER        "beep"
#define TXT_ITEM_SBUZSIG        "beep: signal"
#define TXT_ITEM_STMRSIG        "timer's GPIO"
*/

const char strItemSett[] PROGMEM =
  "backlight""\0"               /* TXT_ITEM_SBRIGH  L=9 */
  "disp. invert""\0"            /* TXT_ITEM_SINV  L=12 */
  "display off""\0"             /* TXT_ITEM_SDISPF  L=11 */
  "info: author""\0"            /* TXT_ITEM_SABOUTA  L=12 */
  "info: version""\0"           /* TXT_ITEM_SABOUTV  L=13 */
  "I2C: frequency""\0"          /* TXT_ITEM_SI2CFRQ  L=14 */
  "I2C: period""\0"             /* TXT_ITEM_SI2CDEL  L=11 */
  "UART: timeout""\0"           /* TXT_ITEM_SUARTTM  L=13 */
  "UART: baudrate""\0"          /* TXT_ITEM_SUARTBD  L=14 */
  "beep""\0"                    /* TXT_ITEM_SBUZZER  L=4 */
  "beep: signal""\0"            /* TXT_ITEM_SBUZSIG  L=12 */
  "timer's GPIO""\0"            /* TXT_ITEM_STMRSIG  L=12 */;

#define TXT_ITEM_SBRIGH         (const char *)(strItemSett + 0)
#define TXT_ITEM_SINV           (const char *)(strItemSett + 10)
#define TXT_ITEM_SDISPF         (const char *)(strItemSett + 23)
#define TXT_ITEM_SABOUTA        (const char *)(strItemSett + 35)
#define TXT_ITEM_SABOUTV        (const char *)(strItemSett + 48)
#define TXT_ITEM_SI2CFRQ        (const char *)(strItemSett + 62)
#define TXT_ITEM_SI2CDEL        (const char *)(strItemSett + 77)
#define TXT_ITEM_SUARTTM        (const char *)(strItemSett + 89)
#define TXT_ITEM_SUARTBD        (const char *)(strItemSett + 103)
#define TXT_ITEM_SBUZZER        (const char *)(strItemSett + 118)
#define TXT_ITEM_SBUZSIG        (const char *)(strItemSett + 123)
#define TXT_ITEM_STMRSIG        (const char *)(strItemSett + 136)

#define TXT_ITEM_SBRIGH_LENGTH  9
#define TXT_ITEM_SINV_LENGTH    12
#define TXT_ITEM_SDISPF_LENGTH  11
#define TXT_ITEM_SABOUTA_LENGTH 12
#define TXT_ITEM_SABOUTV_LENGTH 13
#define TXT_ITEM_SI2CFRQ_LENGTH 14
#define TXT_ITEM_SI2CDEL_LENGTH 11
#define TXT_ITEM_SUARTTM_LENGTH 13
#define TXT_ITEM_SUARTBD_LENGTH 14
#define TXT_ITEM_SBUZZER_LENGTH 4
#define TXT_ITEM_SBUZSIG_LENGTH 12
#define TXT_ITEM_STMRSIG_LENGTH 12

/*******************************************************************************/
/*
#define TXT_HDR_GEN             "Generator"
#define TXT_HDR_PWM             "PWM generator"
#define TXT_HDR_FMETER          "Freq-meter"
#define TXT_HDR_I2C_SCAN        "I2C-scanner"
#define TXT_HDR_UART_BD         "UART baudrate"
#define TXT_HDR_UART_MON        "UART monitor"
#define TXT_HDR_TIMER           "Timer"
#define TXT_HDR_STOPWATCH       "Stopwatch"
#define TXT_HDR_SETT            "Settings"
*/

const char strHeader[] PROGMEM =
  "Generator""\0"               /* TXT_HDR_GEN  L=9 */
  "PWM generator""\0"           /* TXT_HDR_PWM  L=13 */
  "Freq-meter""\0"              /* TXT_HDR_FMETER  L=10 */
  "I2C-scanner""\0"             /* TXT_HDR_I2C_SCAN  L=11 */
  "UART baudrate""\0"           /* TXT_HDR_UART_BD  L=13 */
  "UART monitor""\0"            /* TXT_HDR_UART_MON  L=12 */
  "Timer""\0"                   /* TXT_HDR_TIMER  L=5 */
  "Stopwatch""\0"               /* TXT_HDR_STOPWATCH  L=9 */
  "Settings""\0"                /* TXT_HDR_SETT  L=8 */;

#define TXT_HDR_GEN             (const char *)(strHeader + 0)
#define TXT_HDR_PWM             (const char *)(strHeader + 10)
#define TXT_HDR_FMETER          (const char *)(strHeader + 24)
#define TXT_HDR_I2C_SCAN        (const char *)(strHeader + 35)
#define TXT_HDR_UART_BD         (const char *)(strHeader + 47)
#define TXT_HDR_UART_MON        (const char *)(strHeader + 61)
#define TXT_HDR_TIMER           (const char *)(strHeader + 74)
#define TXT_HDR_STOPWATCH       (const char *)(strHeader + 80)
#define TXT_HDR_SETT            (const char *)(strHeader + 90)

#define TXT_HDR_GEN_LENGTH      9
#define TXT_HDR_PWM_LENGTH      13
#define TXT_HDR_FMETER_LENGTH   10
#define TXT_HDR_I2C_SCAN_LENGTH 11
#define TXT_HDR_UART_BD_LENGTH  13
#define TXT_HDR_UART_MON_LENGTH 12
#define TXT_HDR_TIMER_LENGTH    5
#define TXT_HDR_STOPWATCH_LENGTH 9
#define TXT_HDR_SETT_LENGTH     8

/*******************************************************************************/
/*
#define TXT_LBL_START           "START"
#define TXT_LBL_STOP            "STOP"
#define TXT_LBL_RESET           "RESET"
#define TXT_LBL_NODEV           "no devices"
#define TXT_LBL_DETECT          "detecting..."
#define TXT_LBL_NOSIGRX         "no Rx signal"
#define TXT_LBL_NOSIGNAL        "no signal"
#define TXT_LBL_WAVE            "Wave"
#define TXT_LBL_PERIOD          "T, us"
#define TXT_LBL_DUTY            "Duty"
#define TXT_LBL_INIT            "initialization..."
#define TXT_LBL_BUFFER          "buffer"
#define TXT_LBL_HOUR            "hour"
#define TXT_LBL_MINUTE          "min"
#define TXT_LBL_SECOND          "sec"
#define TXT_LBL_EXPIRED         "time is out"
*/

const char strLabels[] PROGMEM =
  "START""\0"                   /* TXT_LBL_START  L=5 */
  "STOP""\0"                    /* TXT_LBL_STOP  L=4 */
  "RESET""\0"                   /* TXT_LBL_RESET  L=5 */
  "no devices""\0"              /* TXT_LBL_NODEV  L=10 */
  "detecting...""\0"            /* TXT_LBL_DETECT  L=12 */
  "no Rx signal""\0"            /* TXT_LBL_NOSIGRX  L=12 */
  "no signal""\0"               /* TXT_LBL_NOSIGNAL  L=9 */
  "Wave""\0"                    /* TXT_LBL_WAVE  L=4 */
  "T, us""\0"                   /* TXT_LBL_PERIOD  L=5 */
  "Duty""\0"                    /* TXT_LBL_DUTY  L=4 */
  "initialization...""\0"       /* TXT_LBL_INIT  L=17 */
  "buffer""\0"                  /* TXT_LBL_BUFFER  L=6 */
  "hour""\0"                    /* TXT_LBL_HOUR  L=4 */
  "min""\0"                     /* TXT_LBL_MINUTE  L=3 */
  "sec""\0"                     /* TXT_LBL_SECOND  L=3 */
  "time is out""\0"             /* TXT_LBL_EXPIRED  L=11 */;

#define TXT_LBL_START           (const char *)(strLabels + 0)
#define TXT_LBL_STOP            (const char *)(strLabels + 6)
#define TXT_LBL_RESET           (const char *)(strLabels + 11)
#define TXT_LBL_NODEV           (const char *)(strLabels + 17)
#define TXT_LBL_DETECT          (const char *)(strLabels + 28)
#define TXT_LBL_NOSIGRX         (const char *)(strLabels + 41)
#define TXT_LBL_NOSIGNAL        (const char *)(strLabels + 54)
#define TXT_LBL_WAVE            (const char *)(strLabels + 64)
#define TXT_LBL_PERIOD          (const char *)(strLabels + 69)
#define TXT_LBL_DUTY            (const char *)(strLabels + 75)
#define TXT_LBL_INIT            (const char *)(strLabels + 80)
#define TXT_LBL_BUFFER          (const char *)(strLabels + 98)
#define TXT_LBL_HOUR            (const char *)(strLabels + 105)
#define TXT_LBL_MINUTE          (const char *)(strLabels + 110)
#define TXT_LBL_SECOND          (const char *)(strLabels + 114)
#define TXT_LBL_EXPIRED         (const char *)(strLabels + 118)

#define TXT_LBL_START_LENGTH    5
#define TXT_LBL_STOP_LENGTH     4
#define TXT_LBL_RESET_LENGTH    5
#define TXT_LBL_NODEV_LENGTH    10
#define TXT_LBL_DETECT_LENGTH   12
#define TXT_LBL_NOSIGRX_LENGTH  12
#define TXT_LBL_NOSIGNAL_LENGTH 9
#define TXT_LBL_WAVE_LENGTH     4
#define TXT_LBL_PERIOD_LENGTH   5
#define TXT_LBL_DUTY_LENGTH     4
#define TXT_LBL_INIT_LENGTH     17
#define TXT_LBL_BUFFER_LENGTH   6
#define TXT_LBL_HOUR_LENGTH     4
#define TXT_LBL_MINUTE_LENGTH   3
#define TXT_LBL_SECOND_LENGTH   3
#define TXT_LBL_EXPIRED_LENGTH  11

/*******************************************************************************/
#endif

/*******************************************************************************/
/*******************************************************************************/




#if (LANGUAGE == 1)     // ���� ���������� �������

/*******************************************************************************/
/*
#define TXT_DELIM_SPACE         " "
#define TXT_DELIM_DOT           "."
#define TXT_DELIM_COLON         ":"
#define TXT_DELIM_SECOND        "�"
#define TXT_DELIM_MS            "��"
#define TXT_DELIM_BODS          "���/�"
#define TXT_DELIM_ZERO1         "0"
#define TXT_DELIM_KHZ           "���"
#define TXT_DELIM_MICROS        "���"
#define TXT_DELIM_BYTE          "����"
#define TXT_DELIM_MULT          " x "
#define TXT_DELIM_X10MS         "0 ��"
*/

const char strDelimiter[] PROGMEM =
  " ""\0"                       /* TXT_DELIM_SPACE  L=1 */
  ".""\0"                       /* TXT_DELIM_DOT  L=1 */
  ":""\0"                       /* TXT_DELIM_COLON  L=1 */
  "�""\0"                       /* TXT_DELIM_SECOND  L=1 */
  "��""\0"                      /* TXT_DELIM_MS  L=2 */
  "���/�""\0"                   /* TXT_DELIM_BODS  L=5 */
  "0""\0"                       /* TXT_DELIM_ZERO1  L=1 */
  "���""\0"                     /* TXT_DELIM_KHZ  L=3 */
  "���""\0"                     /* TXT_DELIM_MICROS  L=3 */
  "����""\0"                    /* TXT_DELIM_BYTE  L=4 */
  " x ""\0"                     /* TXT_DELIM_MULT  L=3 */
  "0 ��""\0"                    /* TXT_DELIM_X10MS  L=4 */;

#define TXT_DELIM_SPACE         (const char *)(strDelimiter + 0)
#define TXT_DELIM_DOT           (const char *)(strDelimiter + 2)
#define TXT_DELIM_COLON         (const char *)(strDelimiter + 4)
#define TXT_DELIM_SECOND        (const char *)(strDelimiter + 6)
#define TXT_DELIM_MS            (const char *)(strDelimiter + 8)
#define TXT_DELIM_BODS          (const char *)(strDelimiter + 11)
#define TXT_DELIM_ZERO1         (const char *)(strDelimiter + 17)
#define TXT_DELIM_KHZ           (const char *)(strDelimiter + 19)
#define TXT_DELIM_HZ            (const char *)(strDelimiter + 20)
#define TXT_DELIM_MICROS        (const char *)(strDelimiter + 23)
#define TXT_DELIM_BYTE          (const char *)(strDelimiter + 27)
#define TXT_DELIM_MULT          (const char *)(strDelimiter + 32)
#define TXT_DELIM_X10MS         (const char *)(strDelimiter + 36)

#define TXT_DELIM_SPACE_LENGTH  1
#define TXT_DELIM_DOT_LENGTH    1
#define TXT_DELIM_COLON_LENGTH  1
#define TXT_DELIM_SECOND_LENGTH 1
#define TXT_DELIM_MS_LENGTH     2
#define TXT_DELIM_BODS_LENGTH   5
#define TXT_DELIM_ZERO1_LENGTH  1
#define TXT_DELIM_KHZ_LENGTH    3
#define TXT_DELIM_MICROS_LENGTH 3
#define TXT_DELIM_BYTE_LENGTH   4
#define TXT_DELIM_MULT_LENGTH   3
#define TXT_DELIM_X10MS_LENGTH  4

/*******************************************************************************/
/*
#define TXT_OFF                 "����"
#define TXT_ON                  "���"
#define TXT_AUTO                "����"
#define TXT_INV                 "��� (���� = 0)"
#define TXT_NOINV               "��� (���� = 1)"
*/

const char strItemMenu[] PROGMEM =
  "����""\0"                    /* TXT_OFF  L=4 */
  "���""\0"                     /* TXT_ON  L=3 */
  "����""\0"                    /* TXT_AUTO  L=4 */
  "��� (���� = 0)""\0"          /* TXT_INV  L=14 */
  "��� (���� = 1)""\0"          /* TXT_NOINV  L=14 */;

#define TXT_OFF                 (const char *)(strItemMenu + 0)
#define TXT_ON                  (const char *)(strItemMenu + 5)
#define TXT_AUTO                (const char *)(strItemMenu + 9)
#define TXT_INV                 (const char *)(strItemMenu + 14)
#define TXT_NOINV               (const char *)(strItemMenu + 29)

#define TXT_OFF_LENGTH          4
#define TXT_ON_LENGTH           3
#define TXT_AUTO_LENGTH         4
#define TXT_INV_LENGTH          14
#define TXT_NOINV_LENGTH        14

/*******************************************************************************/
/*
#define TXT_ITEM_SBRIGH         "������� ����."
#define TXT_ITEM_SINV           "�������� ����."
#define TXT_ITEM_SDISPF         "����. �������"
#define TXT_ITEM_SABOUTA        "����: �����"
#define TXT_ITEM_SABOUTV        "����: ������"
#define TXT_ITEM_SI2CFRQ        "I2C: �������"
#define TXT_ITEM_SI2CDEL        "I2C: ������"
#define TXT_ITEM_SUARTTM        "UART: �������"
#define TXT_ITEM_SUARTBD        "UART: ��������"
#define TXT_ITEM_SBUZZER        "����"
#define TXT_ITEM_SBUZSIG        "����: ������"
#define TXT_ITEM_STMRSIG        "GPIO �������"
*/

const char strItemSett[] PROGMEM =
  "������� ����.""\0"           /* TXT_ITEM_SBRIGH  L=13 */
  "�������� ����.""\0"          /* TXT_ITEM_SINV  L=14 */
  "����. �������""\0"           /* TXT_ITEM_SDISPF  L=13 */
  "����: �����""\0"             /* TXT_ITEM_SABOUTA  L=11 */
  "����: ������""\0"            /* TXT_ITEM_SABOUTV  L=12 */
  "I2C: �������""\0"            /* TXT_ITEM_SI2CFRQ  L=12 */
  "I2C: ������""\0"             /* TXT_ITEM_SI2CDEL  L=11 */
  "UART: �������""\0"           /* TXT_ITEM_SUARTTM  L=13 */
  "UART: ��������""\0"          /* TXT_ITEM_SUARTBD  L=14 */
  "����""\0"                    /* TXT_ITEM_SBUZZER  L=4 */
  "����: ������""\0"            /* TXT_ITEM_SBUZSIG  L=12 */
  "GPIO �������""\0"            /* TXT_ITEM_STMRSIG  L=12 */;

#define TXT_ITEM_SBRIGH         (const char *)(strItemSett + 0)
#define TXT_ITEM_SINV           (const char *)(strItemSett + 14)
#define TXT_ITEM_SDISPF         (const char *)(strItemSett + 29)
#define TXT_ITEM_SABOUTA        (const char *)(strItemSett + 43)
#define TXT_ITEM_SABOUTV        (const char *)(strItemSett + 55)
#define TXT_ITEM_SI2CFRQ        (const char *)(strItemSett + 68)
#define TXT_ITEM_SI2CDEL        (const char *)(strItemSett + 81)
#define TXT_ITEM_SUARTTM        (const char *)(strItemSett + 93)
#define TXT_ITEM_SUARTBD        (const char *)(strItemSett + 107)
#define TXT_ITEM_SBUZZER        (const char *)(strItemSett + 122)
#define TXT_ITEM_SBUZSIG        (const char *)(strItemSett + 127)
#define TXT_ITEM_STMRSIG        (const char *)(strItemSett + 140)

#define TXT_ITEM_SBRIGH_LENGTH  13
#define TXT_ITEM_SINV_LENGTH    14
#define TXT_ITEM_SDISPF_LENGTH  13
#define TXT_ITEM_SABOUTA_LENGTH 11
#define TXT_ITEM_SABOUTV_LENGTH 12
#define TXT_ITEM_SI2CFRQ_LENGTH 12
#define TXT_ITEM_SI2CDEL_LENGTH 11
#define TXT_ITEM_SUARTTM_LENGTH 13
#define TXT_ITEM_SUARTBD_LENGTH 14
#define TXT_ITEM_SBUZZER_LENGTH 4
#define TXT_ITEM_SBUZSIG_LENGTH 12
#define TXT_ITEM_STMRSIG_LENGTH 12

/*******************************************************************************/
/*
#define TXT_HDR_GEN             "���������"
#define TXT_HDR_PWM             "��������� ���"
#define TXT_HDR_FMETER          "����������"
#define TXT_HDR_I2C_SCAN        "I2C-������"
#define TXT_HDR_UART_BD         "�������� UART"
#define TXT_HDR_UART_MON        "������� UART"
#define TXT_HDR_TIMER           "������"
#define TXT_HDR_STOPWATCH       "����������"
#define TXT_HDR_SETT            "���������"
*/

const char strHeader[] PROGMEM =
  "���������""\0"               /* TXT_HDR_GEN  L=9 */
  "��������� ���""\0"           /* TXT_HDR_PWM  L=13 */
  "����������""\0"              /* TXT_HDR_FMETER  L=10 */
  "I2C-������""\0"              /* TXT_HDR_I2C_SCAN  L=10 */
  "�������� UART""\0"           /* TXT_HDR_UART_BD  L=13 */
  "������� UART""\0"            /* TXT_HDR_UART_MON  L=12 */
  "������""\0"                  /* TXT_HDR_TIMER  L=6 */
  "����������""\0"              /* TXT_HDR_STOPWATCH  L=10 */
  "���������""\0"               /* TXT_HDR_SETT  L=9 */;

#define TXT_HDR_GEN             (const char *)(strHeader + 0)
#define TXT_HDR_PWM             (const char *)(strHeader + 10)
#define TXT_HDR_FMETER          (const char *)(strHeader + 24)
#define TXT_HDR_I2C_SCAN        (const char *)(strHeader + 35)
#define TXT_HDR_UART_BD         (const char *)(strHeader + 46)
#define TXT_HDR_UART_MON        (const char *)(strHeader + 60)
#define TXT_HDR_TIMER           (const char *)(strHeader + 73)
#define TXT_HDR_STOPWATCH       (const char *)(strHeader + 80)
#define TXT_HDR_SETT            (const char *)(strHeader + 91)

#define TXT_HDR_GEN_LENGTH      9
#define TXT_HDR_PWM_LENGTH      13
#define TXT_HDR_FMETER_LENGTH   10
#define TXT_HDR_I2C_SCAN_LENGTH 10
#define TXT_HDR_UART_BD_LENGTH  13
#define TXT_HDR_UART_MON_LENGTH 12
#define TXT_HDR_TIMER_LENGTH    6
#define TXT_HDR_STOPWATCH_LENGTH 10
#define TXT_HDR_SETT_LENGTH     9

/*******************************************************************************/
/*
#define TXT_LBL_START           "����"
#define TXT_LBL_STOP            "����"
#define TXT_LBL_RESET           "�����"
#define TXT_LBL_NODEV           "��� ���������"
#define TXT_LBL_DETECT          "��������������..."
#define TXT_LBL_NOSIGRX         "��� ������� Rx"
#define TXT_LBL_NOSIGNAL        "��� �������"
#define TXT_LBL_WAVE            "�����"
#define TXT_LBL_PERIOD          "T, ���"
#define TXT_LBL_DUTY            "�.���."
#define TXT_LBL_INIT            "�������������..."
#define TXT_LBL_BUFFER          "�����"
#define TXT_LBL_HOUR            "���"
#define TXT_LBL_MINUTE          "���"
#define TXT_LBL_SECOND          "���"
#define TXT_LBL_EXPIRED         "����� �����"
*/

const char strLabels[] PROGMEM =
  "����""\0"                    /* TXT_LBL_START  L=4 */
  "����""\0"                    /* TXT_LBL_STOP  L=4 */
  "�����""\0"                   /* TXT_LBL_RESET  L=5 */
  "��� ���������""\0"           /* TXT_LBL_NODEV  L=13 */
  "��������������...""\0"       /* TXT_LBL_DETECT  L=17 */
  "��� ������� Rx""\0"          /* TXT_LBL_NOSIGRX  L=14 */
  "��� �������""\0"             /* TXT_LBL_NOSIGNAL  L=11 */
  "�����""\0"                   /* TXT_LBL_WAVE  L=5 */
  "T, ���""\0"                  /* TXT_LBL_PERIOD  L=6 */
  "�.���.""\0"                  /* TXT_LBL_DUTY  L=6 */
  "�������������...""\0"        /* TXT_LBL_INIT  L=16 */
  "�����""\0"                   /* TXT_LBL_BUFFER  L=5 */
  "���""\0"                     /* TXT_LBL_HOUR  L=3 */
  "���""\0"                     /* TXT_LBL_MINUTE  L=3 */
  "���""\0"                     /* TXT_LBL_SECOND  L=3 */
  "����� �����""\0"             /* TXT_LBL_EXPIRED  L=11 */;

#define TXT_LBL_START           (const char *)(strLabels + 0)
#define TXT_LBL_STOP            (const char *)(strLabels + 5)
#define TXT_LBL_RESET           (const char *)(strLabels + 10)
#define TXT_LBL_NODEV           (const char *)(strLabels + 16)
#define TXT_LBL_DETECT          (const char *)(strLabels + 30)
#define TXT_LBL_NOSIGRX         (const char *)(strLabels + 48)
#define TXT_LBL_NOSIGNAL        (const char *)(strLabels + 63)
#define TXT_LBL_WAVE            (const char *)(strLabels + 75)
#define TXT_LBL_PERIOD          (const char *)(strLabels + 81)
#define TXT_LBL_DUTY            (const char *)(strLabels + 88)
#define TXT_LBL_INIT            (const char *)(strLabels + 95)
#define TXT_LBL_BUFFER          (const char *)(strLabels + 112)
#define TXT_LBL_HOUR            (const char *)(strLabels + 118)
#define TXT_LBL_MINUTE          (const char *)(strLabels + 122)
#define TXT_LBL_SECOND          (const char *)(strLabels + 126)
#define TXT_LBL_EXPIRED         (const char *)(strLabels + 130)

#define TXT_LBL_START_LENGTH    4
#define TXT_LBL_STOP_LENGTH     4
#define TXT_LBL_RESET_LENGTH    5
#define TXT_LBL_NODEV_LENGTH    13
#define TXT_LBL_DETECT_LENGTH   17
#define TXT_LBL_NOSIGRX_LENGTH  14
#define TXT_LBL_NOSIGNAL_LENGTH 11
#define TXT_LBL_WAVE_LENGTH     5
#define TXT_LBL_PERIOD_LENGTH   6
#define TXT_LBL_DUTY_LENGTH     6
#define TXT_LBL_INIT_LENGTH     16
#define TXT_LBL_BUFFER_LENGTH   5
#define TXT_LBL_HOUR_LENGTH     3
#define TXT_LBL_MINUTE_LENGTH   3
#define TXT_LBL_SECOND_LENGTH   3
#define TXT_LBL_EXPIRED_LENGTH  11

/*******************************************************************************/
#endif


/*******************************************************************************/
/*******************************************************************************/

#endif /* TXT_LABELS_H */
