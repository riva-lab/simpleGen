#include "SSD1306_Fonts.h"


// Название шрифта          Verdana 10 Special
// Автор шрифта             Riva
// Дата и время генерации   01.12.2021 13:45:25
// Сгенерировано            matrixFont v1.1.0.53
// Кодовая страница         1251 (ANSI - кириллица)
// https://gitlab.com/riva-lab/matrixFont

#ifndef FONT_VERDANA_10_SPECIAL_H
#define FONT_VERDANA_10_SPECIAL_H


#ifndef FONT_TYPE_MONOSPACED
#define FONT_TYPE_MONOSPACED            0
#endif
#ifndef FONT_TYPE_PROPORTIONAL
#define FONT_TYPE_PROPORTIONAL          1
#endif


#define FONT_VERDANA_10_SPECIAL_ONLY_ASCII      0 // 1=(7 бит ASCII), 0=(8 бит Win1251)
#define FONT_VERDANA_10_SPECIAL_LENGTH          (224 - FONT_VERDANA_10_SPECIAL_ONLY_ASCII * 128)
#define FONT_VERDANA_10_SPECIAL_START_CHAR      32
#define FONT_VERDANA_10_SPECIAL_CHAR_WIDTH      10
#define FONT_VERDANA_10_SPECIAL_CHAR_HEIGHT     12
#define FONT_VERDANA_10_SPECIAL_FONT_TYPE       (FONT_TYPE_PROPORTIONAL)
#define FONT_VERDANA_10_SPECIAL_ARRAY_STEP      (1 + FONT_VERDANA_10_SPECIAL_CHAR_WIDTH * 2)
#define FONT_VERDANA_10_SPECIAL_ARRAY_LENGTH    (FONT_VERDANA_10_SPECIAL_LENGTH * (1 + FONT_VERDANA_10_SPECIAL_CHAR_WIDTH * 2))

extern const unsigned char font_verdana_10_special[];

extern const DisplayFont_t fontVerdana10Special;

// спецсимволы
#define FONT_CHAR_CLOCK         ((char)128)
#define FONT_CHAR_SETTINGS      ((char)129)
#define FONT_CHAR_SETTINGS_2    ((char)130)
#define FONT_CHAR_SETTINGS_3    ((char)131)
#define FONT_CHAR_WAVE_SQUARE   ((char)134)
#define FONT_CHAR_WAVE_TRIANGLE ((char)135)
#define FONT_CHAR_WAVE_SINUS    ((char)136)
#define FONT_CHAR_GRAPH         ((char)137)
#define FONT_CHAR_HOME          ((char)138)
#define FONT_CHAR_INFO          ((char)147)
#define FONT_CHAR_HELP          ((char)148)
#define FONT_CHAR_OK            ((char)150)
#define FONT_CHAR_CANCEL        ((char)151)
#define FONT_CHAR_DATA          ((char)152)
#define FONT_CHAR_CUBE          ((char)154) // 0x9A
#define FONT_CHAR_BATTERY(x)    ((char)((uint8_t)156 + x))
#define FONT_CHAR_CHARGING      ((char)165)
#define FONT_CHAR_DELTA         ((char)166)
#define FONT_CHAR_HEIGHT        ((char)172)
#define FONT_CHAR_MAN           ((char)173)
#define FONT_CHAR_DEGREE        ((char)176) // 0xB0
#define FONT_CHAR_PLUSMINUS     ((char)177)
#define FONT_CHAR_MICRO         ((char)181)
#define FONT_CHAR_ARROW_L       ((char)139)
#define FONT_CHAR_ARROW_R       ((char)155)
#define FONT_CHAR_ENTER         ((char)190)
#define FONT_CHAR_PROGRESS(x)   ((char)((uint8_t)140 + x))

#endif /* FONT_VERDANA_10_H */



// Название шрифта          Verdana 10 Bold Digits
// Автор шрифта             Riva
// Дата и время генерации   02.12.2021 13:38:18
// Сгенерировано            matrixFont v1.1.0.53
// Кодовая страница         1251 (ANSI - кириллица)
// https://gitlab.com/riva-lab/matrixFont

#ifndef FONT_VERDANA_10_BOLD_DIGITS_H
#define FONT_VERDANA_10_BOLD_DIGITS_H

#ifndef FONT_TYPE_MONOSPACED
#define FONT_TYPE_MONOSPACED                        0
#endif

#ifndef FONT_TYPE_PROPORTIONAL
#define FONT_TYPE_PROPORTIONAL                      1
#endif

#define FONT_VERDANA_10_BOLD_DIGITS_LENGTH          26
#define FONT_VERDANA_10_BOLD_DIGITS_START_CHAR      32
#define FONT_VERDANA_10_BOLD_DIGITS_CHAR_WIDTH      15
#define FONT_VERDANA_10_BOLD_DIGITS_CHAR_HEIGHT     14
#define FONT_VERDANA_10_BOLD_DIGITS_FONT_TYPE       (FONT_TYPE_PROPORTIONAL)
#define FONT_VERDANA_10_BOLD_DIGITS_ARRAY_STEP      (1 + FONT_VERDANA_10_BOLD_DIGITS_CHAR_WIDTH * 2)
#define FONT_VERDANA_10_BOLD_DIGITS_ARRAY_LENGTH    (FONT_VERDANA_10_BOLD_DIGITS_LENGTH * (1 + FONT_VERDANA_10_BOLD_DIGITS_CHAR_WIDTH * 2))

extern const unsigned char font_verdana_10_special[];

extern const DisplayFont_t fontVerdana10Digits;

#endif // FONT_VERDANA_10_BOLD_DIGITS_H 
