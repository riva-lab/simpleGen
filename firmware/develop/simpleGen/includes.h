#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include <stdint.h>

/*******************************************************************************/

// библиотеки
#include <avr/pgmspace.h>
#include <SPI.h>
#include <TimerOnePWM.h>
#include <fastIO.h>
#include <SimpleTimeout.h>
#include <SimpleMenu.h>
#include <SimpleDigitalScale.h>
#include <SimpleSSD1306.h>
#include <SimpleClock.h>
#include <MD_AD9833.h>
#include <GyverPower.h>

/*******************************************************************************/

// включаемые файлы проекта
#include "buttons.h"
#include "connections.h"
#include "devices.h"
#include "font.h"
#include "gui.h"
#include "handlers.h"
#include "parList.h"
#include "sett_item.h"
#include "settings.h"
#include "uartBD.h"
#include "freq_meter.h"
#include "txt/txt1251.h"

/*******************************************************************************/

#ifdef FAST_IO_IMPLEMENTED

#define _pinMode        fastPinMode
#define _digitalWrite   fastDigitalWrite
#define _digitalRead    fastDigitalRead
#define _shiftOut       fastShiftOut

#else

#define _pinMode        pinMode
#define _digitalWrite   digitalWrite
#define _digitalRead    digitalRead
#define _shiftOut       shiftOut

#endif
