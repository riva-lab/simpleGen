#include "gui.h"

/*******************************************************************************/

uint8_t             bc, bp, bh; // коды кнопок
MenuData_t        * menuData;
uint8_t             menuHomeIndex;
uint8_t             generatorEnabled = 0;
SimpleTimeout       timeoutDialog(TIME_DIALOG_REPEAT_KEYS);
SimpleTimeout       timeoutArrows(TIME_ARROWS_ANIMATION);

//                      число цифр              мин макс        нач.значение
SimpleDigitalScale  dsp(PWM_SCALE_PERIOD_SIZE,  0, 8000000L,    1000    );
SimpleDigitalScale  dsd(PWM_SCALE_DUTY_SIZE,    0, 1024,        512     );
SimpleDigitalScale  dsg(GENERATOR_SCALE_SIZE,   0, 120000000UL, 10000   );

SimpleClock         clock;

/*******************************************************************************/
/*******************************************************************************/

uint8_t menuButtonsControl() {
  btn.poll();
  static uint8_t disableButtons;
  disableButtons = isSleepMode();

  if (btn.isActivated()) {
    powerOffTimersReset(1);

    if (disableButtons) btn.waitForRelease();
    else {
      bc = btn.clicked();
      bp = btn.pressed();
      bh = btn.holded();

      if (timeoutDialog.expired()) {
        timeoutDialog.restart();
        if (bh != BUTTON_NONE ) bc = bh;
      }

      return 1;
    }
  }
  else
    powerOffTimersReset(0);

  return 0;
}

/*******************************************************************************/
/*******************************************************************************/

void drawDisplayPrepare() {
  disp.textScale(1);
  disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_ADD + BRUSH_BF_NOFILL);
  disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
  disp.clear();
  disp.setFont(&fontVerdana10Special);
}

void drawWaveSymbol(char ch, uint8_t length) {
  for (uint8_t i = 0; i < length; i++) {
    disp.print(ch);
    disp.setCursorXdec();
  }
}

void drawControlLabel(uint8_t enabled) {
  disp.print(getStringPGM(enabled ? TXT_LBL_STOP : TXT_LBL_START), JUSTIFY_CENTER, JUSTIFY_DOWN);
}

void drawHex(uint8_t data) {
  const uint8_t digits = 2;
  for (uint8_t i = 0; i < digits; i++) {
    uint8_t nibble = (data >> (4 * (digits - 1 - i))) & 0xF;
    char ch = nibble < 10 ? '0' + nibble : 'A' - 10 + nibble;
    disp.print(ch);
  }
}

void drawBin(uint8_t data) {
  for (uint8_t i = 0; i < 8; i++) {
    char ch = '0' + (data >> (7 - i)) & 1;
    disp.print(ch);
  }
}

void drawScaleCursor() {
  uint8_t x, y;
  disp.getCursor(&x, &y);
  disp.setCursorYinc();
  disp.print((char)39);
  disp.setCursor(x, y);
}

void drawProgressBar64(uint8_t y, uint8_t position) {
  disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_ADD + BRUSH_BF_NOFILL);
  disp.rectangle(30, y, SSD1306_WIDTH - 31, 10 + y);
  disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_ADD + BRUSH_BF_FILL);
  disp.rectangle(32, y + 2, SSD1306_WIDTH - 32 - 64 + position, 8 + y);
}

void drawArrowsAnimation() {
  static uint8_t arrowOffset = 1;

  if (timeoutArrows.expired()) {
    timeoutArrows.restart();
    --arrowOffset &= 3;
  }

  disp.print(FONT_CHAR_ARROW_L, 2 + arrowOffset, JUSTIFY_DOWN);
  disp.print(FONT_CHAR_ARROW_R, JUSTIFY_RIGHT + 2 + arrowOffset, JUSTIFY_DOWN);
}

void drawHomeGenerator(uint8_t x, uint8_t y) {
  if (generatorEnabled) {
    disp.setCursor(x, y);
    drawWaveSymbol((char)(FONT_CHAR_WAVE_SQUARE - 1 + generatorEnabled), 3);
  } else
    disp.lineSimpleXY(LINE_HORZ, x, y + disp.textHeight() / 2, 3 * disp.lengthChr(FONT_CHAR_WAVE_SQUARE));
}

void drawHomeBattery(uint8_t x, uint8_t y) {
  uint8_t blinkSymbol = (millis() / 1024) & 1;
  uint8_t level = liIonBatteryPercent();

  if (x == JUSTIFY_RIGHT) {
    x  = SSD1306_WIDTH - 4;
    x -= disp.lengthNum(level);
    x -= disp.lengthChr('%');
  }

  disp.setCursor(x, y);
  //  disp.print(' ');
  disp.print(level);
  disp.print('%');

  x -= disp.lengthChr(' ');
  x -= disp.lengthChr(FONT_CHAR_BATTERY(0));
  disp.setCursor(x, y);

  if (powerIsCharging() && blinkSymbol)
    disp.print(FONT_CHAR_CHARGING);
  else if ((level > BATTERY_LOW) || blinkSymbol) {
    uint8_t battLevel = level / 12;
    if (battLevel > 8) battLevel = 8;
    disp.print(FONT_CHAR_BATTERY(battLevel));
  }
}

void drawUARTBaudrate(uint32_t baud, uint8_t y) {
  uint8_t x = SSD1306_WIDTH - 3;
  x -= disp.lengthChr(' ');
  x -= disp.lengthStr(getStringPGM(TXT_DELIM_BODS));
  x -= disp.lengthNum(baud);
  x /= 2;
  disp.print(baud, x, y);
  disp.print(' ');
  disp.print(getStringPGM(TXT_DELIM_BODS));
}

void drawUARTBufferSize(uint8_t y) {
  uint8_t x = SSD1306_WIDTH - 3;
  x -= disp.lengthChr(' ') * 2;
  x -= disp.lengthStr(getStringPGM(TXT_DELIM_BYTE));
  x -= disp.lengthStr(getStringPGM(TXT_LBL_BUFFER));
  x -= disp.lengthNum((uint16_t)(UART_TERM_BUFFER));
  x /= 2;

  disp.setCursor(x, y);
  disp.print(getStringPGM(TXT_LBL_BUFFER));
  disp.print(' ');
  disp.print((uint16_t)(UART_TERM_BUFFER));
  disp.print(' ');
  disp.print(getStringPGM(TXT_DELIM_BYTE));
}

//void drawDec(uint16_t data, uint8_t width) {
//  uint16_t d = data;
//  do d /= 10, width--; while (d && width);
//  while (width--) disp.print('0');
//  disp.print(data);
//}

void setClockCursor(uint8_t is_ms) {
  uint8_t x = SSD1306_WIDTH;
  x -= (disp.lengthChr('0') + 1) * (6 + is_ms * 3);
  x -= (disp.lengthChr(',') + 1) * (2 + is_ms);
  x++;
  x /= 2;

  disp.setCursor(x, 1);
}

void drawClock(uint8_t h, uint8_t m, uint8_t s, uint16_t ms) {
  uint8_t is_ms = (ms < 1000) ? 1 : 0;
  setClockCursor(is_ms);
  disp.setDigits(2);
  disp.print(h);
  disp.print('#');
  disp.print(m);
  disp.print('#');
  disp.print(s);

  if (is_ms) {
    disp.setDigits(3);
    disp.print(',');
    disp.print(ms);
  }

  disp.setDigits();
}

void checkRange(uint8_t *data, int8_t min, int8_t max) {
  if ((int8_t)(*data) > max) *data = (uint8_t)min;
  if ((int8_t)(*data) < min) *data = (uint8_t)max;
}

uint8_t inRange(uint8_t data, int8_t min, int8_t max) {
  return ((int8_t)(data) >= min) && ((int8_t)(data) <= max);
}

// вывод шкалы и подсказки
void drawScale(SimpleDigitalScale *sc, uint8_t cursor, uint8_t min, uint8_t size, uint8_t incr, char *hint) {
  uint8_t is_in_range = inRange(cursor, min + 1, min + size);

  // вывод подсказки и/или единицы измерения
  if (is_in_range) {
    sc->positionSet(min + size - cursor);

    if (incr == uint8_t(+1)) sc->inc();
    if (incr == uint8_t(-1)) sc->dec();

    uint8_t x, y;
    disp.getCursor(&x, &y);
    disp.setFont(&fontVerdana10Special);
    disp.setCursor(1, JUSTIFY_DOWN);
    disp.print(hint);
    disp.setCursor(x, y);
  }

  // вывод шкалы
  disp.setFont(&fontVerdana10Digits);
  sc->digitReset(1);
  while (sc->digitAvailable()) {
    if (sc->isCurrentDigit() && is_in_range) drawScaleCursor();
    disp.print(sc->digit());
    if ((sc->digits() > 4) && (sc->digitAvailable() % 3 == 0)) disp.print(' ');
  }
}

/*******************************************************************************/
/*******************************************************************************/

// управление генератором сигналов на AD9833
void drawScreenGenerator() {
  uint8_t           cursor;
  uint8_t           index   = 8;
  uint8_t           incr    = 0;
  static uint8_t    wave    = 2;

  do {
    if (isSleepMode())
      waitInSleep();
    else {
      drawDisplayPrepare();
      drawControlLabel(generatorEnabled);

      // вывод переключателя формы сигнала
      checkRange(&wave, 0, 2);
      disp.setCursor(2, 1);
      drawWaveSymbol((char)(FONT_CHAR_WAVE_SQUARE + wave), 3);

      // вывод подсказки и/или единицы измерения
      disp.setCursor(1, JUSTIFY_DOWN);
      checkRange(&cursor, 1, GENERATOR_SCALE_SIZE + GENERATOR_WAVE_SIZE);
      if (cursor > GENERATOR_SCALE_SIZE) {
        disp.print(getStringPGM(TXT_LBL_WAVE));
        disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_XOR + BRUSH_BF_FILL);
        disp.rectangle(1, 1, 32, 12);
      } else {
        dsg.positionSet(GENERATOR_SCALE_SIZE - cursor);
        disp.print(F("F, "));
        disp.print(getStringPGM(TXT_DELIM_HZ));
      }

      // вывод шкалы установки частоты
      disp.setFont(&fontVerdana10Digits);
      uint8_t x = SSD1306_WIDTH;
      dsg.digitReset(0);
      while (dsg.digitAvailable()) {
        uint8_t f = dsg.isCurrentDigit() && (cursor <= GENERATOR_SCALE_SIZE);
        uint8_t d = dsg.digit();
        disp.setCursor(x -= disp.lengthNum(d) + 1, 1);
        if (f) drawScaleCursor();
        disp.print(d);
        if (dsg.digitAvailable() == GENERATOR_SCALE_SIZE - 2) disp.print(',');
        if (dsg.digitAvailable() % 3 == 2) x -= disp.lengthChr(',') + 1;
      }
      //    drawScale(&dsg, cursor, 1, GENERATOR_SCALE_SIZE, incr, getStringPGM(TXT_LBL_FREQ));
      disp.setFont(&fontVerdana10Special);

      disp.repaint();
      incr = 0;
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bp == BUTTON_OK     ) break;
      if (bc == BUTTON_OK     ) generatorEnabled = !generatorEnabled;
      if (bc == BUTTON_UP     )
          if (cursor > GENERATOR_SCALE_SIZE) wave++; else dsg.inc();
      if (bc == BUTTON_DOWN   )
          if (cursor > GENERATOR_SCALE_SIZE) wave--; else dsg.dec();
      if (bc == BUTTON_LEFT   ) cursor--;
      if (bc == BUTTON_RIGHT  ) cursor++;
    }

    // установка параметров генератора
    genSettings(wave, dsg.value(), generatorEnabled);
  } while (1);

  if (generatorEnabled)
    generatorEnabled = 1 + wave;
}

// генератор ШИМ-сигнала
void drawScreenPWMGenerator() {
  uint8_t changed   = 0;
  uint8_t enable    = 0;
  uint8_t incr      = 0;
  uint8_t cursor    = PWM_SCALE_PERIOD_SIZE;

  Serial.end(); // превентивно отключаем вывод Tx UART
  //  freqMeterDisable(); // превентивно выключаем частотомер

  do {
    if (!isSleepMode()) {
      drawDisplayPrepare();
      drawControlLabel(enable);

      checkRange(&cursor, 1, PWM_SCALE_DUTY_OFFSET + PWM_SCALE_DUTY_SIZE);

      // вывод шкалы периода
      disp.setCursor(1, 1);
      drawScale(&dsp, cursor, PWM_SCALE_PERIOD_OFFSET, PWM_SCALE_PERIOD_SIZE, incr, getStringPGM(TXT_LBL_PERIOD));

      // вывод шкалы коэффициента заполнения
      disp.setDigits(4);
      disp.setCursor(SSD1306_WIDTH - 1 - disp.lengthNum((uint16_t)dsd.value()), 1);
      disp.setDigits();
      drawScale(&dsd, cursor, PWM_SCALE_DUTY_OFFSET, PWM_SCALE_DUTY_SIZE, incr, getStringPGM(TXT_LBL_DUTY));

      disp.repaint();
      incr = 0;
    }

    if (enable) {
      if (changed) {
        changed = 0;
        Timer1PWM.initialize(dsp.value());      // период в мкс
        Timer1PWM.pwm(PIN_PWM, dsd.value());    // вывод, заполнение
      }
    } else {
      changed = 1;
      Timer1PWM.stop();
      Timer1PWM.disablePwm(PIN_PWM);
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bp == BUTTON_OK     ) break;
      if (bc == BUTTON_OK     ) enable = !enable;
      if (bc == BUTTON_UP     ) changed = 1, incr++;
      if (bc == BUTTON_DOWN   ) changed = 1, incr--;
      if (bc == BUTTON_LEFT   ) cursor--;
      if (bc == BUTTON_RIGHT  ) cursor++;
    }
  } while (1);

  Timer1PWM.stop();
  Timer1PWM.disablePwm(PIN_PWM);
  pinMode(PIN_PWM, INPUT);
}

// частотомер
void drawScreenFrequencyMeter() {
  uint8_t   enable  = 0;
  uint8_t   w, k, x = 0;
  char      m;
  float     f       = 0;

  SimpleTimeout timeoutFMeterInit(TIME_FREQ_METER_INIT / 64);

  freqMeterEnable();    // включение и инициализация схемы частотомера

  timeoutFMeterInit.restart();
  while (x < 64) {      // ждем окончания инициализации схемы частотомера 5с
    if (timeoutFMeterInit.expired()) {
      timeoutFMeterInit.restart();
      drawDisplayPrepare();
      drawProgressBar64(5, x++);
      disp.print(getStringPGM(TXT_LBL_INIT), JUSTIFY_CENTER, JUSTIFY_DOWN);
      disp.repaint();
    }
  }

  do {
    if (isSleepMode())
      waitInSleep();
    else {
      if (enable) {
        enable = 0;

        drawDisplayPrepare();
        disp.print(getStringPGM(TXT_LBL_DETECT), JUSTIFY_CENTER, JUSTIFY_CENTER);
        disp.repaint();
        delay(50);

        freqMeterMeasure(); // запуск измерения частоты сигнала (блокирующий)

        f = freqMeterValue();
        m = 0;
        if (f > (FREQ_METER_FMAX + 5000)) m = '>', f = FREQ_METER_FMAX;

        k = 0, w = 3;
        while (f > 999.9) f /= 1000.0, k++;
        float tmp = f;
        while (tmp > 9.99) tmp /= 10.0, w--;

        x  = SSD1306_WIDTH;
        x -= disp.lengthNum(f, w);
        x -= disp.lengthChr(' ');
        if (k)    x -= disp.lengthStr(getStringPGM(TXT_DELIM_KHZ));
        else      x -= disp.lengthStr(getStringPGM(TXT_DELIM_HZ));
        if (m) x -= disp.lengthChr(m) + disp.lengthChr(' ');
        x /= 2;
      }

      drawDisplayPrepare();
      drawControlLabel(enable);

      if (f > 0.001) {
        disp.setCursor(x, 1);
        if (m) disp.print(m), disp.print(' ');
        disp.print(f, w);
        disp.print(' ');

        if (k)    disp.print(getStringPGM(TXT_DELIM_KHZ));
        else      disp.print(getStringPGM(TXT_DELIM_HZ));
      } else
        disp.print(getStringPGM(TXT_LBL_NOSIGNAL), JUSTIFY_CENTER, 1);

      disp.repaint();
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bc == BUTTON_OK     ) enable = !enable;
    }
  } while (bp != BUTTON_OK);
  freqMeterDisable(); // выключение частотомера
}

// сканер устройств на шине I2C
void drawScreenI2CScanner() {
  uint8_t enable = 0, count = 0, index;
  uint8_t addr[8];

  do {
    drawDisplayPrepare();

    if (enable) {
      count = 0;
      index = 0;

      // процесс сканирования на частоте 100 кГц
      for (uint8_t address = 1; address <= 127; address++) {
        if (address == SSD1306_I2C_ADDRESS) continue;

        delay(settI2CDelayHandler(0, 0));
        Wire.setClock(10000UL * (uint8_t)settI2CFreqHandler(0, 0));
        Wire.beginTransmission(address);
        uint8_t error = Wire.endTransmission();
        if (!error) addr[count++] = address;
        delay(1);

        Wire.setClock(1000UL * DISPLAY_I2C_SPEED);
        drawProgressBar64(10, address / 2);
        disp.repaint();

        if (count >= 8) break;
      }
      enable = 0;
    }

    if (isSleepMode())
      waitInSleep();
    else {
      if (!count) {
        disp.print(getStringPGM(TXT_LBL_NODEV), JUSTIFY_CENTER, 1);
        disp.print(getStringPGM(TXT_LBL_START), JUSTIFY_CENTER, JUSTIFY_DOWN);
      }
      else
        for (uint8_t i = 0; i < count; i++) {
          if (i == index) {
            disp.setCursor(1, JUSTIFY_DOWN);
            drawBin(addr[i]);
            disp.print('b');
            disp.setCursor(SSD1306_WIDTH / 2, JUSTIFY_DOWN);
            disp.print(addr[i]);
            disp.print('d');
            disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_ULINE);
          }
          else
            disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
          disp.setCursor(1 + i * 16, 0);
          drawHex(addr[i]);
        }
      disp.repaint();
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bc == BUTTON_OK     ) enable = !enable;
      if (bc == BUTTON_LEFT   ) index--;
      if (bc == BUTTON_RIGHT  ) index++;

      checkRange(&index, 0, count - 1);
    }
  } while (bp != BUTTON_OK);
}

// детектор скорости потока UART
void drawScreenUARTScanner() {
  uint8_t enable = 0;

  do {
    drawDisplayPrepare();

    if (isSleepMode())
      waitInSleep();
    else {
      if (enable) {
        enable = 0;
        disp.print(getStringPGM(TXT_LBL_DETECT), JUSTIFY_CENTER, JUSTIFY_CENTER);
        disp.repaint();
        delay(50);
        uartBaudrateDetectorGo(IRQ_UART_RX, 1000 * (uint8_t)settUARTtimeoutHandler(0, 0));
      }

      uint32_t b = uartBaudrateDetectorValue();
      if (b)  drawUARTBaudrate(b, 0);
      else    disp.print(getStringPGM(TXT_LBL_NOSIGRX), JUSTIFY_CENTER, 1);
      disp.print(getStringPGM(TXT_LBL_START), JUSTIFY_CENTER, JUSTIFY_DOWN);
      disp.repaint();
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bc == BUTTON_OK     ) enable = !enable;
    }
  } while (bp != BUTTON_OK);
}

// UART монитор
void drawScreenUARTMonitor() {
  uint8_t   opened  = 0;
  uint16_t  index   = 0, count = 0;
  uint8_t   str     = 0, width = UART_TERM_WIDTH_CH;

  static uint8_t enable = 0;

  uint8_t data[UART_TERM_BUFFER];

  SimpleTimeout timeoutUart(300);

  // показываем установленную скорость
  drawDisplayPrepare();
  drawUARTBaudrate(settGetUARTbaudrateValue(), 1);
  drawUARTBufferSize(JUSTIFY_DOWN);
  disp.repaint();
  delay(1000);

  pinMode(PIN_PWM, INPUT); // превентивно отключаем ШИМ генератор
  do {
    uint8_t strings = UART_TERM_BUFFER / width + UART_TERM_BUFFER % width;
    float cursor_w = ((float)(SSD1306_WIDTH - 6) / (float)(strings - 1));

    if (enable) {
      if (!opened) opened = 1, Serial.begin(settGetUARTbaudrateValue());
      timeoutUart.restart();

      // прием данных в буфер
      while (Serial.available() && !timeoutUart.expired()) {
        if (count < UART_TERM_BUFFER - 1) count++;
        data[index++] = Serial.read();
        index %= UART_TERM_BUFFER;
      }
    } else {
      opened = 0;
      Serial.end();
    }

    if (!isSleepMode()) {
      drawDisplayPrepare();

      // отображение данных
      uint16_t j = 0;
      if (count == UART_TERM_BUFFER - 1) j = index;
      j += str * width;
      j %= UART_TERM_BUFFER;
      for (uint16_t i = 0; i < width * 2; i++) {
        if ((count < UART_TERM_BUFFER - 1) && (j >= count)) break;

        if (i % width == 0)
          disp.setCursor(0, disp.textHeight() * (i / width));

        if (width == UART_TERM_WIDTH_CH) {
          if      (data[j] == '\n')           disp.print(FONT_CHAR_ENTER);
          else if (data[j] < (uint8_t)(' '))  disp.print((char)127);
          else                                disp.print((char)data[j]);
        }
        else {
          drawHex((uint8_t)data[j]);
          disp.print(' ');
        }

        if (++j >= UART_TERM_BUFFER) j = 0;
      }

      // ползунок положения, мигает при остановке приема
      if (enable || (millis() % 500 < 250)) {
        disp.rectangle(0, SSD1306_HEIGHT - 5, SSD1306_WIDTH - 1, SSD1306_HEIGHT - 1);
        disp.lineSimpleXY(LINE_HORZ, 2 + (uint8_t)(cursor_w * str), SSD1306_HEIGHT - 3, (uint8_t)cursor_w + 2);
        //      disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_ADD + BRUSH_BF_NOFILL);
        //      disp.rectangle(0, 0, SSD1306_WIDTH - 1, SSD1306_HEIGHT - 1);
      }
      disp.repaint();
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bp == BUTTON_OK     ) break;
      if (bc == BUTTON_OK     ) enable = !enable;
      if (bc == BUTTON_UP     ) if (str) str--;
      if (bc == BUTTON_DOWN   ) if (str < strings - 2) str++;
      if (bc == BUTTON_LEFT   ) width = UART_TERM_WIDTH_CH;
      if (bc == BUTTON_RIGHT  ) width = UART_TERM_WIDTH_HEX;

      if (str >= strings - 2) str = strings - 2;
    }
  } while (1);

  Serial.end();
}

// управление выводом GPIO таймера
void timerGpioControl(uint8_t enable) {
  enable++;
  if (setting.timerSignalType) {
    _pinMode(PIN_GPIO, OUTPUT);
    _digitalWrite(PIN_GPIO, setting.timerSignalType != enable);
  }
}

// таймер
void drawScreenTimer() {
  uint8_t   cursor;
  uint8_t   enable = 0;
  uint8_t   expire = 0;
  uint8_t   buzzer = 0;
  uint8_t   incr   = 0;
  uint8_t   m = 0, h = 0, s = 0;
  uint16_t  ms     = 0;

  SimpleDigitalScale  dsh(TIMER_SCALE_H_SIZE, 0, 99UL, setting.timerHours  );
  SimpleDigitalScale  dsm(TIMER_SCALE_M_SIZE, 0, 59UL, setting.timerMinutes);
  SimpleDigitalScale  dss(TIMER_SCALE_S_SIZE, 0, 59UL, setting.timerSeconds);

  timerGpioControl(0);

  do {
    buzzerUpdate();

    // логика работы
    if (enable) {
      clock.update();

      if (expire = clock.expired()) {
        timerGpioControl(0);
        if (!buzzer) buzzer = 1, buzzerStart(setting.timerBuzzBeeps, setting.timerBuzzPulseX10);
        clock.stop();
      } else {
        timerGpioControl(1);
        buzzerStop();
        buzzer = 0;
        clock.remain(&h, &m, &s, &ms);
      }
    } else {
      timerGpioControl(0);
      buzzerStop();
    }

    // отрисовка графического интерфейса
    if (!isSleepMode()) {
      drawDisplayPrepare();
      drawControlLabel(enable);
      disp.setFont(&fontVerdana10Digits);

      if (enable) {
        if (expire) {
          disp.setFont(&fontVerdana10Special);
          disp.print(getStringPGM(TXT_LBL_EXPIRED), JUSTIFY_CENTER, 1);
        }
        else
          drawClock(h, m, s, (uint16_t)(-1));
      } else {
        checkRange(&cursor, 1, TIMER_SCALE_S_OFFSET + TIMER_SCALE_S_SIZE);

        setClockCursor(0);
        drawScale(&dsh, cursor, TIMER_SCALE_H_OFFSET, TIMER_SCALE_H_SIZE, incr, getStringPGM(TXT_LBL_HOUR));
        disp.print('#');
        drawScale(&dsm, cursor, TIMER_SCALE_M_OFFSET, TIMER_SCALE_M_SIZE, incr, getStringPGM(TXT_LBL_MINUTE));
        disp.print('#');
        drawScale(&dss, cursor, TIMER_SCALE_S_OFFSET, TIMER_SCALE_S_SIZE, incr, getStringPGM(TXT_LBL_SECOND));

        clock.timer(dsh.value(), dsm.value(), dss.value());

        setting.timerHours    = dsh.value();
        setting.timerMinutes  = dsm.value();
        setting.timerSeconds  = dss.value();
      }

      disp.repaint();
      incr = 0;
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bc == BUTTON_OK     ) (enable = !enable) ? clock.reset(), clock.resume() : clock.stop();
      if (bc == BUTTON_UP     ) incr++;
      if (bc == BUTTON_DOWN   ) incr--;
      if (bc == BUTTON_LEFT   ) cursor--;
      if (bc == BUTTON_RIGHT  ) cursor++;
    }
  } while (bp != BUTTON_OK);

  _pinMode(PIN_GPIO, INPUT);
  buzzerStop();
  settingsSave();
}

// секундомер
void drawScreenStopwatch() {
  uint8_t   trigger = 0;
  uint8_t   enable  = 0;
  uint8_t   m = 0, h = 0, s = 0;
  uint16_t  ms      = 0;

  clock.stop();
  clock.reset();

  _pinMode(PIN_GPIO, INPUT_PULLUP);
  delay(10);
  uint8_t gpio_last = _digitalRead(PIN_GPIO);

  do {
    if (trigger) {
      trigger = 0;
      (enable = !enable) ? clock.resume() : clock.stop();
      buzzerStart(1, 5);
    }

    buzzerUpdate();

    clock.update();
    clock.time(&h, &m, &s, &ms);

    if (!isSleepMode()) {
      drawDisplayPrepare();
      disp.print(getStringPGM(TXT_LBL_RESET), JUSTIFY_RIGHT + 1, JUSTIFY_DOWN);
      drawControlLabel(enable);
      disp.setFont(&fontVerdana10Digits);
      drawClock(h, m, s, ms);
      disp.repaint();
    }

    // запуск/останов по изменению уровня GPIO
    uint8_t gpio;
    if ((gpio = _digitalRead(PIN_GPIO)) != gpio_last) {
      gpio_last = gpio;
      trigger = 1;
    }

    // обработка нажатий кнопок
    if (menuButtonsControl()) {
      if (bc == BUTTON_OK     ) trigger = 1;
      if (bc == BUTTON_RIGHT  ) clock.reset();
    }
  } while (bp != BUTTON_OK);
}

/*******************************************************************************/

// mode: 0=length, 1=print, 2=print selected
uint8_t printParam(Parameter_t *p, uint8_t mode, float value) {

  if (p->type == PARAM_TYPE_EMPTY) return 0;

  uint8_t length = 0;

  if (mode > 1)
    disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_ULINE);

  if (p->type == PARAM_TYPE_NUM2)
    disp.setDigits(2);

  if (p->type < PARAM_TYPE_STR) {
    if (mode)   disp.print(value, p->length);
    else        length += disp.lengthNum(value, p->length) + 2;
    value = 0;
    disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
  }

  if (p->step == 0) value = 0;
  if ((p->type != PARAM_TYPE_EMPTY) && p->str) {
    char *s = getStringFromParamStruct(p, uint8_t(value));
    if (mode)   disp.print(s);
    else        length += disp.lengthStr(s) + 2;
  }

  disp.setDigits();
  disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);

  return length;
}

void drawParameters(Parameter_t *p, uint8_t next, uint8_t change) {
  static float value[PARAM_DLG_MAX] = {0};

  static uint8_t index = 0;
  if (next) index++;

  //  disp.textScale(2);
  uint8_t x = SSD1306_WIDTH;
  for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {

    if (p[i].handler)
      value[i] = p[i].handler(0, 0);

    if (p[i].step) {
      if (i == index) {
        if (change == 0) value[i] -= p[i].step;
        if (change == 2) value[i] += p[i].step;
      }

      if (value[i] > p[i].max) value[i] = p[i].min;
      if (value[i] < p[i].min) value[i] = p[i].max;

      if ((i == index) && (change != 1) && (p[i].handler))
        p[i].handler(1, value[i]);

    } else if (i == index) index++;

    if (index > PARAM_DLG_MAX - 1) index = 0;

    x -= printParam(p + i, 0, value[i]);
  }

  if (x > SSD1306_WIDTH) x = 0; else x /= 2;
  disp.setCursor(x, 0);
  for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {
    x = 1;

    if (i == index) {
      if (p[i].step) x = 2;
      else if (++index > PARAM_DLG_MAX - 1) index = 0;
    }

    printParam(p + i, x, value[i]);
  }
  //  disp.textScale(1);
}

/*******************************************************************************/
/*******************************************************************************/

uint8_t menuItemControl(uint8_t item, uint8_t itemTop) {
  static uint8_t next = 0, change = 1;

  if (isSleepMode())
    waitInSleep();
  else {
    // самостоятельные пункты главного меню, без подменю
    switch (menuHomeIndex) {
      case HOME_MENU_ITEM_GEN:        drawScreenGenerator();      break;
      case HOME_MENU_ITEM_PWM:        drawScreenPWMGenerator();   break;
      case HOME_MENU_ITEM_FMETER:     drawScreenFrequencyMeter(); break;
      case HOME_MENU_ITEM_I2C_SCAN:   drawScreenI2CScanner();     break;
      case HOME_MENU_ITEM_UART_BD:    drawScreenUARTScanner();    break;
      case HOME_MENU_ITEM_UART_MON:   drawScreenUARTMonitor();    break;
      case HOME_MENU_ITEM_TIMER:      drawScreenTimer();          break;
      case HOME_MENU_ITEM_STOPWTCH:   drawScreenStopwatch();      break;
    }

    settingItem_t *settItem = getItemStructPGM(menuData->items + item);
    dialog_t      *dlg      = 0;

    if (settItem == NULL) return MENU_CONTROL_EXIT;

    drawDisplayPrepare();
    disp.print(getStringPGM(settItem->caption), JUSTIFY_CENTER, JUSTIFY_DOWN);
    drawArrowsAnimation();

    if (settItem->dialog) {
      dlg = getDlgStructPGM(settItem->dialog);
      drawParameters(dlg->param, next, change);
      settingsSave();
    }
    next    = 0;
    change  = 1;
    disp.repaint();
  }

  if (menuButtonsControl()) {
    if (bh == BUTTON_LEFT   ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT_TRY;

    if (bp == BUTTON_OK     ) return MENU_CONTROL_EXIT;

    next = (bc == BUTTON_OK);
    if (bc == BUTTON_LEFT   ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT;
    if (bc == BUTTON_UP     ) change++;
    if (bc == BUTTON_DOWN   ) change--;
  }

  return MENU_CONTROL_NO;
}

uint8_t menuHomeControl(uint8_t item, uint8_t itemTop) {
  if (isSleepMode())
    waitInSleep();
  else {
    menuData = getMenuDataPGM(menuHome, item);

    drawDisplayPrepare();
    disp.print(getStringPGM(menuData->caption), JUSTIFY_CENTER, JUSTIFY_DOWN);
    drawArrowsAnimation();
    drawHomeGenerator(1, 0);
    drawHomeBattery(JUSTIFY_RIGHT, 0);
    disp.repaint();
  }

  if (menuButtonsControl()) {
    if (bh == BUTTON_OK     ) return MENU_CONTROL_NO;
    if (bh == BUTTON_LEFT   ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT_TRY;
    if (bh == BUTTON_UP     ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_DOWN   ) return MENU_CONTROL_NEXT_TRY;

    if (bc == BUTTON_OK     ) return MENU_CONTROL_ENTER;
    if (bc == BUTTON_LEFT   ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT;
    if (bc == BUTTON_UP     ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_DOWN   ) return MENU_CONTROL_NEXT;
  }

  return MENU_CONTROL_NO;
}

/*******************************************************************************/
/*******************************************************************************/

void menuHomeEnter(uint8_t item) {
  menuHomeIndex = item;
  menuData = getMenuDataPGM(menuHome, item);

  SimpleMenu menu;
  menu.begin(0, menuData->count - 1);
  menu.onControl(menuItemControl);
  menu.slidingTime(500);
  menu.exitTime(0);
  menu.show();
}

void screenHome() {
  SimpleMenu menu;
  menu.begin(0, HOME_MENU_ITEMS - 1);
  menu.onEnter(menuHomeEnter);
  menu.onControl(menuHomeControl);
  menu.slidingTime(500);
  menu.exitTime(0);
  menu.show();
}
