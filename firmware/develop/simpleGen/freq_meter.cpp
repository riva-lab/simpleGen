#include "freq_meter.h"

/*******************************************************************************/

#define REG_BIT_1(reg,nbit)     ((reg) |=  (1 << (nbit)))
#define REG_BIT_0(reg,nbit)     ((reg) &= ~(1 << (nbit)))
#define REG_BIT_TEST(reg,nbit)  ((reg) &   (1 << (nbit)))

/*******************************************************************************/

#define TMR1_INT_OVF_ON         REG_BIT_1(TIFR1, TOV1), REG_BIT_1(TIMSK1, TOIE1)
#define TMR1_INT_OVF_OFF        REG_BIT_0(TIMSK1, TOIE1)
#define TMR1_INT_CAPT_ON        REG_BIT_1(TIFR1, ICF1), REG_BIT_1(TIMSK1, ICIE1)
#define TMR1_INT_CAPT_OFF       REG_BIT_0(TIMSK1, ICIE1)
#define TMR1_STOP               (TCCR1B &= ~0b00000111)
#define TMR1_START(ps)          (TCCR1B |= ((ps) & 0b00000111))

#define COMP_INCAP_ENABLE       REG_BIT_1(ACSR, ACIC)
#define COMP_INCAP_DISABLE      REG_BIT_0(ACSR, ACIC)

/*******************************************************************************/

#define TMR1_FREQ_PS1           1
#define TMR1_FREQ_PS8           2
#define TMR1_FREQ_PS64          3
#define TMR1_FREQ_PS256         4
#define TMR1_FREQ_PS1024        5

#define TMR1_FREQ_MAX           (float(float(F_CPU) / ((uint16_t)(1UL << 16) - 1)))
#define TMR1_CAPTURES           2

/*******************************************************************************/

volatile uint8_t    captureCount;
volatile uint16_t   timestamp[TMR1_CAPTURES] = { 0 };
volatile uint16_t  *timestampPtr;

const uint8_t       tmr1prescalerShift[] = { 0, 0, 3, 6, 8, 10 };
float               freqTimer, freqSignal;

SimpleTimeout       timeoutFreqMeter(FREQ_METER_TIMEOUT);

/*******************************************************************************/

void        freqMeterTimerEnable(uint8_t prescaler);
void        freqMeterTimerDisable();
uint16_t    freqMeterTryMeasureDelta(uint8_t prescaler);

/*******************************************************************************/

// включение и инициализация схемы частотомера
void freqMeterEnable() {
  freqSignal = 0;

  _pinMode(PIN_FREQ_IN, INPUT);     // вход - сигнал
  _pinMode(PIN_FREQ_PWR, OUTPUT);   // вывод стабилизатора схемы смещения
  _digitalWrite(PIN_FREQ_PWR, 1);   // вкл. стабилизатор схемы смещения

  //                        ***     настройка компаратора
  REG_BIT_0(ADCSRB, ACME);          // инверсный вход компаратора - AIN1
  REG_BIT_0(DIDR1, AIN1D);          // выключить входной цифровой буфер на входе AIN1
  REG_BIT_1(ACSR, ACBG);            // неинверсный вход компаратора - ИОН 1,1В
  COMP_INCAP_ENABLE;                // включить вход захвата

  //                        ***     настройка таймера 1
  TCCR1A = 0;
  TCCR1B = 0;                       // таймер выключен
  TCCR1C = 0;
  TMR1_INT_OVF_OFF;                 // прерывание по переполнению выключено
  REG_BIT_1(TCCR1B, ICNC1);         // вкл. блок антидребезга захвата
}

// выключение частотомера
void freqMeterDisable() {
  //  _pinMode(PIN_FREQ_IN, INPUT);
  _pinMode(PIN_FREQ_PWR, INPUT);    // выкл. стабилизатор схемы смещения

  COMP_INCAP_DISABLE;               // вход захвата выключен
  freqMeterTimerDisable();          // выключаем таймер
}

// включение таймера 1 с заданным предделителем
void freqMeterTimerEnable(uint8_t prescaler) {
  freqTimer = float(F_CPU)  / ((uint16_t)1 << tmr1prescalerShift[prescaler]);
  TCNT1     = 0;
  TMR1_START(prescaler);            // включаем таймер
  TMR1_INT_CAPT_ON;                 // прерывание по захвату включено
}

// выключение таймера 1
void freqMeterTimerDisable() {
  TMR1_INT_CAPT_OFF;                // прерывание по захвату выключено
  TMR1_STOP;                        // выключаем таймер
}

// прерывание при захвате нового значения
ISR(TIMER1_CAPT_vect) {
  *(timestampPtr++) = ICR1;

  if (--captureCount == 0)
    TMR1_INT_CAPT_OFF;
}

// измерение периода сигнала (в тиках таймера)
uint16_t freqMeterTryMeasureDelta(uint8_t prescaler) {
  captureCount  = TMR1_CAPTURES;    // взводим счетчик захваченных временных меток
  timestampPtr  = timestamp;        // начинать запись временных меток в начало массива

  timeoutFreqMeter.restart();       // взводим таймер таймаута (не ждать же вечно вх. сигнал?)

  freqMeterTimerEnable(prescaler);  // включаем таймер с заданным предделителем

  // ждем окончания захвата 2 временных меток
  while (captureCount && !timeoutFreqMeter.expired());

  freqMeterTimerDisable();          // выключаем таймер

  return timestamp[1] - timestamp[0];
}

// запуск измерения частоты сигнала (блокирующий)
void freqMeterMeasure() {

  // похоже кто-то забыл включить частотомер - делать тут нечего
  if (!REG_BIT_TEST(ACSR, ACIC)) return;

  // начинаем с нуля
  freqSignal = 0;

  // начинаем замер, предполагая самую низкую частоту
  uint8_t ps = TMR1_FREQ_PS1024;

  // перебыраем в цикле все предделители от большего к меньшему
  while (ps) {

    uint8_t noPrescaler = (ps == TMR1_FREQ_PS1);
    uint8_t count       = 0;

    // устанавливаем макс. число замеров для усреднения
    uint8_t countMax    = (freqSignal > 10000.0) ? 100 : 1;

    // начинаем с нуля - а как иначе-то
    freqSignal = 0;

    while (1) {

      // пытаемся измерить период сигнала
      uint16_t delta = freqMeterTryMeasureDelta(ps);

      // сработал таймаут ожидания сигнала, сигнала нет, уходим
      if (timeoutFreqMeter.expired()) {
        freqSignal = 0;
        return;
      }

      if (noPrescaler) {

        // частота больше предельной -> все кончено
        if (timestamp[0] == timestamp[1]) {
          freqSignal = float(F_CPU);
          return;
        }

        // что-то пошло не так при замере, повторяем
        if (timestamp[0] == 0) continue;
      }

      // похоже, частота выше предельной, уходим на замер с меньшим предделителем
      if (timestamp[1] == timestamp[0]) break;

      // расчет частоты текущего замера и накопление данных
      if (delta) freqSignal += freqTimer / (float(delta) - 0.05);

      // еще один замер завершен, отмечаем это
      count++;

      // продолжение замеров на F>10k
      if (noPrescaler && (count <= countMax)) continue;

      // выход из цикла
      break;
    }

    // расчет усредненной частоты
    freqSignal /= count / 1.0003;

    // конец, если измеренная частота ниже минимума следующего частотного диапазона
    float freqNextRangeMin = TMR1_FREQ_MAX / ((uint16_t)1 << tmr1prescalerShift[--ps]);
    if (freqSignal < freqNextRangeMin) return;
  }
}

// значение частоты сигнала
float freqMeterValue() {
  return freqSignal;
}
