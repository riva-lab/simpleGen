#include "sett_item.h"
#include <avr/pgmspace.h>

/*******************************************************************************/

dialog_t        dialogOut;
settingItem_t   settingItemOut;
MenuData_t      menuDataOut;
char            strOut[PARAM_STR_BUFFER];

/*******************************************************************************/
/*     доступ к структурам, хранящимся в Flash (использование PROGMEM)         */
/*******************************************************************************/

dialog_t * getDlgStructPGM(const dialog_t *dialog PROGMEM) {
  if (dialog == NULL) return NULL;
  memcpy_P(&dialogOut, dialog, sizeof(dialog_t));
  return &dialogOut;
}

settingItem_t * getItemStructPGM(const settingItem_t *siArr PROGMEM) {
  if (siArr == NULL) return NULL;
  memcpy_P(&settingItemOut, siArr, sizeof(settingItem_t));
  return &settingItemOut;
}

MenuData_t * getMenuDataPGM(const MenuData_t *menu PROGMEM, uint8_t index) {
  if (menu == NULL) return NULL;
  memcpy_P(&menuDataOut, menu + index, sizeof(MenuData_t));
  return &menuDataOut;
}

char * getStringPGM(const char *strArr PROGMEM) {
  if (strArr == NULL) return NULL;
  memcpy_P(strOut, strArr, sizeof(strOut));
  return strOut;
}

char * getStringPGMTable(void *strArr PROGMEM, uint8_t index) {
  if (strArr == NULL) return NULL;
  memcpy_P(strOut, pgm_read_word( (const char* const*)strArr + index), sizeof(strOut));
  return strOut;
}

char * getStringFromParamStruct(Parameter_t *p, uint8_t index) {
  if (p == NULL) return NULL;
  if ((p->type >= PARAM_TYPE_STR) && (p->min != p->max))
    return getStringPGMTable(p->str, index);
  else
    return getStringPGM(p->str);
}
