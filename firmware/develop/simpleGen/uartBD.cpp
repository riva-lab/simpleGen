#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include "uartBD.h"

/*******************************************************************************/

volatile uint8_t    uartIsFast = 1;
volatile uint8_t    uartFronts;
volatile uint8_t    uartFinish;
volatile uint8_t    uartIRQNumber;
volatile uint16_t   uartTS[64];
volatile uint16_t * uartTSPtr;
volatile uint32_t   uartBaud;
volatile uint16_t   uartTimeout;

/*******************************************************************************/

uint8_t     uartBaudrateDetectorGo(uint16_t timeout_ms);
void        uartBaudrateDetectorStop();
uint32_t    uartBaudrateDetectorValue();

/*******************************************************************************/

// обработчик прерывания по изменению уровня порта
void uartPulseIRQHandler() {
  if (uartFronts) {
    --uartFronts;
    *(uartTSPtr++) = TCNT1;
  }
  else {
    uartBaud = 0;
    uartBaudrateDetectorStop();
  }
}

// прерывание по переполнению таймера 1
ISR(TIMER1_OVF_vect) {
  if (uartTimeout) uartTimeout--;
  else {
    uartBaud    = 1;
    uartBaudrateDetectorStop();
    uartFinish  = 2;
  }
}

/*******************************************************************************/

// запуск процесса опредения скорости
uint8_t uartBaudrateDetectorGo(uint8_t irq_number, uint16_t timeout_ms) {
  uartFronts    = UART_BD_SAMPLES;
  uartFinish    = 0;
  uartTSPtr     = uartTS;
  uartIRQNumber = irq_number;

  for (uint8_t i = 0; i < UART_BD_SAMPLES; i++) uartTS[i] = 0;

  noInterrupts();
  Serial.end();
  TIMSK0   &= ~_BV(TOIE0);  // отключить прерывания timer0 overflow

  TCCR1A    = 0;
  TCCR1B    = 0;
  TCNT1     = 0;
  TIMSK1    = (1 << TOIE1); // включить прерывание timer1 overflow

  if (uartIsFast) {
    TCCR1B |= (1 << CS10);
    if (timeout_ms)
      uartTimeout = timeout_ms / (uint8_t)(UINT16_MAX / 16000UL);
  }
  else {
    TCCR1B |= (1 << CS10) | (1 << CS11);
    if (timeout_ms)
      uartTimeout = timeout_ms / (uint16_t)(64UL * UINT16_MAX / 16000UL);
  }

  attachInterrupt(uartIRQNumber, uartPulseIRQHandler, CHANGE);
  interrupts();

  while (!uartFinish);
  return uartBaud > 0;
}

// остановка процесса накопления данных и расчет
void uartBaudrateDetectorStop() {
  if (!uartBaud) {
    uint32_t minValue = UINT16_MAX;

    for (uint8_t i = 0; i < UART_BD_SAMPLES - 1; i++) {
      // скорости больше 128 кбод/с корректно не анализируются
      if ((uartTS[i + 1] < 100) || (uartTS[i] < 100)) continue;

      if (uartTS[i + 1] < uartTS[i])    uartTS[i] = 0x10000UL + uartTS[i + 1] - uartTS[i];
      else                              uartTS[i] = uartTS[i + 1] - uartTS[i];

      if (uartTS[i] < minValue)         minValue = uartTS[i];
    }

    minValue = (uint32_t)(1.2 * minValue);

    uint8_t count = 0;
    uint32_t summ = 0;
    for (uint8_t i = 1; i < UART_BD_SAMPLES - 1; i++)
      if (uartTS[i] <= minValue) summ += uartTS[i], count++;

    uartBaud = (uint32_t)(16000000.0 * count / (double)summ);
    if (!uartIsFast) uartBaud /= 64;
  }

  if (uartIsFast && (uartBaud < 20000)) {
    uartIsFast = 0;
    uartBaudrateDetectorGo(uartIRQNumber, 0);
  } else {
    detachInterrupt(uartIRQNumber);
    TCCR1A      = 0;
    TCCR1B      = 0;

    uartFinish  = 1;
    uartIsFast  = 1;

    TIMSK0     |= _BV(TOIE0); // восстановление прерываний timer0 overflow
  }
}

// вычисленное значение скорости
uint32_t uartBaudrateDetectorValue() {
  if (uartFinish == 1)
    return uartBaud;
  return 0;
}
