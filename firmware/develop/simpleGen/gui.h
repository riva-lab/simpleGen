#ifndef GUI_H
#define GUI_H

/*******************************************************************************/

#include "includes.h"

/*******************************************************************************/

// таймауты
#define TIME_DIALOG_REPEAT_KEYS 100     // [мс] время повтора ввода при зажатой кнопке в диалоге
#define TIME_ARROWS_ANIMATION   200     // [мс] период кадров анимации стрелок
#define TIME_FREQ_METER_INIT    5000    // [мс] время инициализации схемы частотомера

// парамерты монитора UART rx
#define UART_TERM_WIDTH_CH      16      // [символы] ширина строки для вывода в виде ASCII
#define UART_TERM_WIDTH_HEX     6       // [символы] ширина строки для вывода в виде HEX
#define UART_TERM_BUFFER        350     // [байты] размер приемного буфера

// размеры и смещения символов шкал
#define GENERATOR_SCALE_SIZE    9       // [символы] размер шкалы частоты генератора
#define GENERATOR_WAVE_SIZE     1       // [символы] размер переключателя формы сигнала генератора
#define PWM_SCALE_PERIOD_SIZE   7       // [символы] размер шкалы периода таймера для ШИМ
#define PWM_SCALE_DUTY_SIZE     4       // [символы] размер шкалы заполнения таймера для ШИМ
#define PWM_SCALE_PERIOD_OFFSET 0
#define PWM_SCALE_DUTY_OFFSET   (PWM_SCALE_PERIOD_OFFSET + PWM_SCALE_PERIOD_SIZE)
#define TIMER_SCALE_H_SIZE      2       // [символы] размер шкалы таймера - часы 
#define TIMER_SCALE_M_SIZE      2       // [символы] размер шкалы таймера - минуты
#define TIMER_SCALE_S_SIZE      2       // [символы] размер шкалы таймера - секунды
#define TIMER_SCALE_H_OFFSET    0
#define TIMER_SCALE_M_OFFSET    (TIMER_SCALE_H_OFFSET + TIMER_SCALE_H_SIZE)
#define TIMER_SCALE_S_OFFSET    (TIMER_SCALE_M_OFFSET + TIMER_SCALE_M_SIZE)

#define HOME_MENU_ITEMS         9       // кол-во пунктов главного меню

// индексы пунктов главного меню
#define HOME_MENU_ITEM_GEN      0       // генератор на AD9833
#define HOME_MENU_ITEM_PWM      1       // генератор ШИМ
#define HOME_MENU_ITEM_FMETER   2       // частотомер
#define HOME_MENU_ITEM_I2C_SCAN 3       // сканер шины I2C
#define HOME_MENU_ITEM_UART_BD  4       // детектор скорости UART
#define HOME_MENU_ITEM_UART_MON 5       // монитор UART rx
#define HOME_MENU_ITEM_TIMER    6       // таймер
#define HOME_MENU_ITEM_STOPWTCH 7       // секундомер
#define HOME_MENU_ITEM_SETTINGS 8       // настройки

/*******************************************************************************/

void screenHome();

/*******************************************************************************/

#endif /* GUI_H */
