#ifndef HANDLERS_H
#define HANDLERS_H

/*******************************************************************************/

#include "includes.h"

/*******************************************************************************/
/*                         Обработчики для диалога                             */
/*******************************************************************************/

float settBrightnessHandler(uint8_t rw_mode, float newValue);
float settInversionHandler(uint8_t rw_mode, float newValue);
float settDisplayOffHandler(uint8_t rw_mode, float newValue);
float settI2CFreqHandler(uint8_t rw_mode, float newValue);
float settI2CDelayHandler(uint8_t rw_mode, float newValue);
float settUARTbaudHandler(uint8_t rw_mode, float newValue);
float settUARTtimeoutHandler(uint8_t rw_mode, float newValue);
float settBuzzerHandler(uint8_t rw_mode, float newValue);
float settBuzzPulseX10Handler(uint8_t rw_mode, float newValue);
float settBuzzBeepsHandler(uint8_t rw_mode, float newValue);
float settTimerGPIOHandler(uint8_t rw_mode, float newValue);

uint32_t settGetUARTbaudrateValue();

/*******************************************************************************/

#endif /* HANDLERS_H */
