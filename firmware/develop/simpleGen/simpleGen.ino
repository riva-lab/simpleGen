#include "includes.h"

void setup() {
  settingsRestore();        // восстановить настройки
  powerOffTimersReset(1);   // сброс таймеров автовыключения
  commonInit();             // общая инициализация
}

void loop() {
  screenHome();             // отобразить домашний экран-меню
}
