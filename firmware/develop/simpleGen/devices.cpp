#include "devices.h"

/*******************************************************************************/

SimpleSSD1306       disp;
CustomButtons       btn;
MD_AD9833           gen(PIN_AD9833_FSYNC); // Hardware SPI

SimpleTimeout       timeoutGetVCC(TIME_BATTERY);
SimpleTimeout       timeoutSecond(1000);

uint8_t             sleepModeStatus = 0;

SimpleTimeout       timeoutBuzzer;
uint8_t             buzzerBeeps;
uint8_t             buzzerActive;

/*******************************************************************************/

// общая инициализация
void commonInit() {
  buzzerStop();

  disp.begin(SSD1306_I2C_ADDRESS, DISPLAY_I2C_SPEED / 10);
  disp.repaint();
  //  disp.setFont(&fontVerdana10Special);

  btn.begin();
  btn.addButton(PIN_BUTTON_UP, BUTTON_UP);
  btn.addButton(PIN_BUTTON_DOWN, BUTTON_DOWN);
  btn.addButton(PIN_BUTTON_LEFT, BUTTON_LEFT);
  btn.addButton(PIN_BUTTON_RIGHT, BUTTON_RIGHT);
  btn.addButton(PIN_BUTTON_OK, BUTTON_OK);

  gen.begin();
  gen.setMode(MD_AD9833::MODE_OFF);

  _pinMode(PIN_CHARGING_STATE, INPUT_PULLUP);

  freqMeterDisable();
  settBrightnessHandler(0, 0);
  settInversionHandler(0, 0);

  Serial.begin(UART_BAUDRATE);
}


uint8_t powerIsCharging() {
  return _digitalRead(PIN_CHARGING_STATE) == 0;
}

void powerOffTimersReset(uint8_t reset) {
  static uint8_t t2;

  if (reset) {
    if (t2 = setting.timeDisplayOff) {
      sleepModeStatus = 0;
      disp.power(1);
    }
  }

  if (timeoutSecond.expired()) {
    timeoutSecond.restart();

    if (setting.timeDisplayOff) {
      if (t2) {
        t2--;
      } else {
        sleepModeStatus = 1;
        disp.power(0);
      }
    }
  }
}

uint8_t isSleepMode() {
  return sleepModeStatus;
}

void waitInSleep() {
  power.hardwareDisable(PWR_ALL);
  power.sleepDelay(100);
  power.hardwareEnable(PWR_ALL);
}


uint16_t getSystemVoltage() {
  static uint16_t v;
  if (timeoutGetVCC.expired()) {
    timeoutGetVCC.restart();

    //reads internal 1V1 reference against VCC
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);  // For ATmega328
    uint16_t adc = 0;
    for (uint8_t i = 0; i < 32; i++) {
      delayMicroseconds(200);           // Wait for Vref to settle
      ADCSRA |= _BV(ADSC);              // Start conversion
      while (bit_is_set(ADCSRA, ADSC)); // measuring
      uint8_t low  = ADCL;              // must read ADCL first - it then locks ADCH
      uint8_t high = ADCH;              // unlocks both
      adc += (high << 8) | low;
    }
    v = 1023UL * 32 * ADC_UREF_CALIBRATED / adc; // расчёт реального VCC
  }
  // фильтр
  //  static uint16_t f = 3700;
  //  f = (float)f * (15.0 / 16.0) + (1023UL * 32 / 16 * vcc_const / adc);
  return v; // возвращает VCC в милливольтах
}

uint8_t liIonBatteryPercent() {
  uint16_t volts = getSystemVoltage();
  uint16_t capacity;
  if (volts > LIION_VOLTAGE_80) capacity = map(volts, LIION_VOLTAGE_100, LIION_VOLTAGE_80, 100, 80);
  else if ((volts <= LIION_VOLTAGE_80) && (volts > LIION_VOLTAGE_60) ) capacity = map(volts, LIION_VOLTAGE_80, LIION_VOLTAGE_60, 80, 60);
  else if ((volts <= LIION_VOLTAGE_60) && (volts > LIION_VOLTAGE_40) ) capacity = map(volts, LIION_VOLTAGE_60, LIION_VOLTAGE_40, 60, 40);
  else if ((volts <= LIION_VOLTAGE_40) && (volts > LIION_VOLTAGE_20) ) capacity = map(volts, LIION_VOLTAGE_40, LIION_VOLTAGE_20, 40, 20);
  else if (volts <= LIION_VOLTAGE_20) capacity = map(volts, LIION_VOLTAGE_20, LIION_VOLTAGE_0, 20, 0);
  capacity = constrain(capacity, 0, 100);
  return capacity;
}


// управление работой генератора
void genSettings(uint8_t wave, uint32_t freq, uint8_t enable) {
  MD_AD9833::mode_t mode = MD_AD9833::MODE_OFF;
  if (enable)
    switch (wave) {
      case 0: mode = MD_AD9833::MODE_SQUARE1;     break;
      case 1: mode = MD_AD9833::MODE_TRIANGLE;    break;
      case 2: mode = MD_AD9833::MODE_SINE;        break;
    }
  else wave = 255;

  static uint8_t wave_p;
  static uint32_t freq_p;

  if (wave_p != wave)
    gen.setMode(mode);

  if (freq_p != freq) {
    gen.setFrequency(MD_AD9833::CHAN_0, (float)freq / 10.0);
    gen.setActiveFrequency(MD_AD9833::CHAN_0);
  }

  wave_p = wave;
  freq_p = freq;
}


void buzzerActivate(uint8_t enable) {
  if (enable && setting.buzzerEnable) {
    Timer1PWM.initialize(500);      // период в мкс, 2 кГц
    Timer1PWM.pwm(PIN_BUZZER, 512); // вывод, заполнение
  } else {
    Timer1PWM.stop();
    Timer1PWM.disablePwm(PIN_BUZZER);
    _pinMode(PIN_BUZZER, OUTPUT);
    _digitalWrite(PIN_BUZZER, 0);
  }
}

void buzzerStart(uint8_t beeps, uint8_t durationX10ms) {
  if (buzzerActive) return;
  if (beeps < 0x7F) buzzerBeeps = beeps * 2 - 1;

  buzzerActivate(buzzerActive = 1);
  timeoutBuzzer.begin(durationX10ms * 10);
}

void buzzerStop() {
  buzzerActivate(buzzerActive = 0);
}

void buzzerUpdate() {
  if (!buzzerActive) return;

  if (timeoutBuzzer.expired()) {
    timeoutBuzzer.restart();
    if (--buzzerBeeps)
      buzzerActivate(buzzerBeeps & 1);
    else
      buzzerStop();
  }
}
